package awesomechat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;

import awesomechat.io.StreamHelper;
import awesomechat.net.hi.client.ConsoleClient;
import awesomechat.net.hi.server.AwesomeServer;
import awesomechat.util.channelmanagement.Channel;
import awesomechat.util.channelmanagement.ChannelManager;
import awesomechat.util.usermanagement.User;
import awesomechat.util.usermanagement.UserManager;
import logging.Appender;
import logging.LogLevel;
import logging.Logger;

public class EntryPoint {
	
	public static void main(String[] args) throws IOException, InterruptedException {
		final Logger logger = Logger.getLogger("default");
		final Appender appender = new Appender(System.out, LogLevel.PRODUCTIVE);
		logger.addAppender(appender);
		
		SessionStore.put(StreamHelper.class, new StreamHelper());
		
		boolean serverMode = false;
		String host = "localhost";
		int port = AwesomeServer.DEFAULT_PORT;
		
		for (final Arg arg : Arg.toArgList(args)) {
			switch (arg.getName()) {
				case "server":
				case "s":
					serverMode = true;
					break;
				case "port":
				case "p":
					if (arg.getValue() != null) {
						try {
							port = Integer.parseInt(arg.getValue());
						} catch (final NumberFormatException ex) {
							logger.warn("Could not use port %s, using default port instead", arg.getValue());
						}
					}
					break;
				case "host":
				case "h":
					if (arg.getValue() != null) {
						host = arg.getValue();
					} else {
						logger.warn("When using host switch (-h --host), give it a value");
					}
					break;
				case "loglevel":
					if (arg.getValue() != null) {
						try {
							final LogLevel logLevel = LogLevel.valueOf(arg.getValue());
							appender.setLogLevel(logLevel);
						} catch (final IllegalArgumentException e) { }
					}
					break;
					
			}
		}
		
		if (serverMode) {
			final User serverUser = new User("server", "SERVER");
			
			logger.info("Starting in server mode");
			
			logger.debug("Initializing UserManager");
			SessionStore.put(UserManager.class, new UserManager());
			
			logger.debug("Initializing ChannelManager");
			SessionStore.put(ChannelManager.class, new ChannelManager(new Channel("Foyer", null, serverUser, true)));

			logger.debug("Starting server");
			final AwesomeServer server = new AwesomeServer();
			server.start(port);
			
			final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			String line;
			while (!"stop".equals(line = reader.readLine())) {
				args = line.split(" ");
				final int l = args.length;
				
				if (l > 0 && args[0].startsWith("/")) {
					switch (args[0].substring(1)) {
						case "list":
							if (l > 1) {
								switch (args[1]) {
									case "users":
										final UserManager um = SessionStore.get(UserManager.class);
										final Iterator<User> ui = um.users();
										int userCount = 0;
										while (ui.hasNext()) {
											final User u = ui.next();
											logger.info(u.toString());
											userCount++;
										}
										logger.info("%s Users listed", userCount);
										break;
									case "channels":
										final ChannelManager cm = SessionStore.get(ChannelManager.class);
										final Iterator<Channel> ci = cm.channels();
										int channelCount = 0;
										boolean deep = false;
										if (l > 2) {
											if ("deep".equals(args[2])) {
												deep = true;
											}
										}
										while (ci.hasNext()) {
											final Channel c = ci.next();
											logger.info(c.toString());
											if (deep) {
												final Iterator<User> joined = c.joinedUsers();
												while (joined.hasNext()) {
													logger.info("   %s", joined.next());
												}
											}
											channelCount++;
										}
										logger.info("%s Channels listed", channelCount);
										break;
								}
							}
							break;
					}
				}
			}
			reader.close();
			
			server.stop();
		} else {
			logger.info("Starting in client mode");
			
			final ConsoleClient client = new ConsoleClient(host, port);
			
			try {
				client.start();
				// Start is blocking the current thread, when the chat sends the exit command, the client will be closed
				client.close();
			} catch (final IOException ex) {
				logger.info("Could not connect: " + ex.getMessage());
			}
		}
	}
}