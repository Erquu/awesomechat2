package awesomechat;

import java.util.List;
import java.util.Vector;

public class Arg {
	/**
	 * Parses the arguments from the main method (string-array) into objects<br />
	 * It will parse --[name] as a whole word and -[char] as characters (each char as a<br />
	 * different {@link Arg}.<br />
	 * Example:<br />
	 * --hallo => Arg[name:"hallo",value:null]<br />
	 * --hallo welt => Arg[name:"hallo",value:"welt"]<br />
	 * -ha 12 => Arg[name:"h",value:null], Arg[name:"a",value:"12"]
	 * @param args The array to parse
	 * @return An array of the parsed arguments
	 */
    public static Arg[] toArgList(final String[] args) {
        final List<Arg> argList = new Vector<Arg>();

        String tmpArg;
        for (final String arg : args) {
            if (arg.startsWith("--")) {
                argList.add(new Arg(arg.substring(2).toLowerCase()));
            } else if (arg.startsWith("-")) {
                for (final char c : arg.substring(1).toCharArray()) {
                    argList.add(new Arg(String.valueOf(c)));
                }
            } else {
                if (argList.size() > 0) {
                    tmpArg = arg;
                    if (arg.startsWith("\""))
                        tmpArg = tmpArg.substring(1);
                    if (arg.endsWith("\""))
                        tmpArg = tmpArg.substring(0, tmpArg.length() - 1);
                    argList.get(argList.size() - 1).value = tmpArg;
                }
            }
        }

        return argList.toArray((new Arg[argList.size()]));
    }

    /** Name of the argument */
    private final String name;
    /** Value of the argument */
    private String value = null;

    /**
     * Creates a new argument
     * @param name The name of the argument
     */
    private Arg(final String name) {
        this.name = name;
    }
    
    /** @return The name of the argument */
    public String getName() {
    	return name;
    }
    
    /** @return The value of the argument */
    public String getValue() {
    	return value;
    }
}