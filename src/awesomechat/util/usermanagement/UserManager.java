package awesomechat.util.usermanagement;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

//TODO: Add serialiation for users
/**
 * Keeps track of all users who connected during the server session
 * 
 * @author Erquu
 */
public final class UserManager {
	/** The default format string for unregistered users */
	private final String unregisteredUserNameFormat = "User #%s";
	/**
	 * Increments on every connect<br />
	 * The <i>unregisteredUserCount</i> is used to give every new user a unique userid 
	 */
	private int unregisteredUserCounter = 0;
	/**
	 * A list of all passwords in MD5 identified by the userid...
	 */
	private final Map<String, String> passwordMD5s = new HashMap<String, String>();
	/**
	 * A list of all users who connected during the server session
	 */
	private final Map<String, User> userList = new HashMap<String, User>();

	/**
	 * Creates a new, unregistered User<br />
	 * The user will not be added to the userList and is marked as OFFLINE
	 * @return The newly created user
	 */
	public User createUser() {
		final User user = new User(String.format(unregisteredUserNameFormat, ++unregisteredUserCounter));
		
		return user;
	}
	
	/**
	 * Adds a user to the user list
	 * @param user The user to be added
	 * @return The added user
	 */
	public User addUser(final User user) {
		if (!userList.containsKey(user.getUserID())) {
			userList.put(user.getUserID(), user);
		}
		return userList.get(user.getUserID());
	}
	
	/**
	 * Removes a user from the userlist
	 * @param user The user to be removed
	 */
	public void removeUser(final User user) {
		if (userList.containsKey(user.getUserID())) {
			userList.remove(user.getUserID());
		}
	}
	
	/**
	 * Tries to log in a user<br />
	 * The new user needs to be in state <i>Offline</i> in order to be able to log in
	 * @param oldUserId The user id of the old user
	 * @param newUserId The user id of the new user
	 * @return The logged in user, if failed, the old user will be returned
	 * @throws UserAlreadyLoggedInException The requested user is already logged in
	 * @throws UserAuthenticationException The password does not match
	 */
	public User loginUser(final String oldUserId, final String newUserId, final String passMD5) throws UserAlreadyLoggedInException, UserAuthenticationException {
		User user = getUser(newUserId);
		boolean success = false;
		if (user == null) {
			user = addUser(new User(newUserId));
			passwordMD5s.put(newUserId, passMD5);
			success = true;
		} else {
			if (passwordMD5s.get(newUserId).equals(passMD5)) {
				if (user.getUserState() == UserState.OFFLINE) {
					user.setUserState(UserState.ONLINE);
					
					success = true;
				} else {
					throw new UserAlreadyLoggedInException("The user is already logged in.");
				}
			} else {
				throw new UserAuthenticationException("Passwords do not match!");
			}
		}
		
		if (success) {
			if (userList.containsKey(oldUserId)) {
				// TODO: Should users be removed or marked as offline?
				userList.remove(oldUserId);
			}
		} else {
			user = getUser(oldUserId);
		}
		
		return user;
	}
	
	/**
	 * Checks if a Displayname is available
	 * @param name The name to be checked
	 * @return true if the username is available
	 */
	public boolean isDisplaynameAvailable(final String name) {
		boolean available = true;
		if (name != null) {
			final Iterator<User> users = userList.values().iterator();
			while (users.hasNext()) {
				if (name.equals(users.next().getDisplayName())) {
					available = false;
					break;
				}
			}
		}
		return available;
	}
	
	/**
	 * Gets a user by its userId
	 * @param userId The userId
	 * @return The found user or null if the user is not in the user list
	 */
	public User getUser(final String userId) {
		if (userList.containsKey(userId)) {
			return userList.get(userId);
		}
		return null;
	}
	
	/**
	 * Returns an iterator of all users
	 * @return An iterator of all users
	 */
	public Iterator<User> users() {
		return userList.values().iterator();
	}
}
