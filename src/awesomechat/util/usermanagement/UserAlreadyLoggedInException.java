package awesomechat.util.usermanagement;

public class UserAlreadyLoggedInException extends UserException {
	private static final long serialVersionUID = 7207218706487640080L;

	public UserAlreadyLoggedInException(final String message) {
		super(message);
	}
}
