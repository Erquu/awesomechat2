package awesomechat.util.usermanagement;

public class UserException extends Exception {
	private static final long serialVersionUID = -7331237603900660006L;

	public UserException(final String message) {
		super(message);
	}
}
