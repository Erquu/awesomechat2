package awesomechat.util.usermanagement;

public class UserAuthenticationException extends UserException {
	private static final long serialVersionUID = 7629263795094718088L;

	public UserAuthenticationException(final String message) {
		super(message);
	}
}
