package awesomechat.util.usermanagement;

import java.net.Socket;

import awesomechat.util.ExtendedHashMap;

public class UserSocketMap extends ExtendedHashMap<User, Socket> {
	private static final long serialVersionUID = -46192298776971900L;
}
