package awesomechat.util.usermanagement;

/**
 * Class representing a User 
 * 
 * @author Erquu
 */
public class User {
	/** The id of the user */
	private final String userid; // TODO: Think of different authentication names
	/** The last known/current state of the user */
	private UserState state = UserState.OFFLINE;
	/** The name visible for other users */
	private String displayName = null;
	
	/**
	 * Creates a new User
	 * @param userid The id of the user
	 */
	public User(final String userid) {
		this.userid = userid;
	}
	/**
	 * Creates a new User
	 * @param userid The id of the user
	 * @param displayName The displayname of the user
	 */
	public User(final String userid, final String displayName) {
		this.userid = userid;
		this.displayName = displayName;
	}
	
	/**
	 * Returns the userid of the user
	 * @return The userid of the user
	 */
	public String getUserID() {
		return userid;
	}
	
	/**
	 * Returns the display name of the user
	 * @return The display name of the user
	 */
	public String getDisplayName() {
		if (displayName == null)
			return userid;
		return displayName;
	}
	
	/**
	 * Sets the state of the user
	 * @param state The current state of the user
	 */
	public void setUserState(final UserState state) {
		this.state = state;
	}
	/**
	 * Returns the state of the user
	 * @return The state of the user
	 */
	public UserState getUserState() {
		return state;
	}
	
	@Override
	public String toString() {
		return String.format("User[userid=%s, displayName=%s, state=%s]", userid, displayName, state);
	}
	
	@Override
	public boolean equals(final Object arg0) {
		if (arg0 == null && !(arg0 instanceof User))
			return false;
		return User.class.cast(arg0).userid.equals(userid);
	}
}
