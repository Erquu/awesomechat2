package awesomechat.util.usermanagement;

/**
 * Enum representing the state of a user
 * 
 * @author Erquu
 */
public enum UserState {
	/** User is online and connected to the server */
	ONLINE,
	/** User is offline */
	OFFLINE,
	
	/** DO NOT DISTURB - User does not want to be disturbed */
	DND,
	/** AWAY FROM KEYBOARD - User ismybe not available at the moment */
	AFK
	
	//CUSTOM
}
