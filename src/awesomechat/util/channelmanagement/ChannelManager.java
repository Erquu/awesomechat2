package awesomechat.util.channelmanagement;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import awesomechat.util.usermanagement.User;

/**
 * Keeps track of all created channels (server role)<br />
 * or all channels, the user has joined successfully (client role)
 * 
 * @author Erquu
 */
public class ChannelManager {
	/** Maps all channels to their names respectively */
	private final Map<String, Channel> channelMap = new HashMap<String, Channel>();
	/** The default channel for every user */
	private Channel defaultChannel = null;
	
	public ChannelManager(final Channel defaultChannel) {
		this.defaultChannel = defaultChannel;
		channelMap.put(defaultChannel.getChannelName(), defaultChannel);
	}
	
	/**
	 * Creates a new Channel
	 * @param name The name of the new channel
	 * @param passMD5 The password of the new channel, if null, the channel has no password
	 * @param creator The creator of the channel
	 * @param addUser Set to true if the creator should directly join the channel
	 * @return The created channel or null if creation failed
	 */
	public Channel createChannel(final String name, final String passMD5, final User creator, final boolean addUser) {
		if (!channelMap.containsKey(name)) {
			Channel c = new Channel(name, passMD5, creator, addUser);
			channelMap.put(name, c);
			return c;
		}
		return null;
	}
	
	/**
	 * @return The default channel for every user
	 */
	public Channel getDefaultChannel() {
		return defaultChannel;
	}
	
	/**
	 * Returns the count of all created channels
	 * @return The count of all created channels
	 */
	public int getChannelCount() {
		return channelMap.size();
	}
	
	/**
	 * Returns an iterator of all created channels
	 * @return An iterator of all created channels
	 */
	public Iterator<Channel> channels() {
		return channelMap.values().iterator();
	}
	
	/**
	 * Returns an iterator of all channels in which a user has joined
	 * @param user The user who joined the channels
	 * @return An iterator of all channels in which a user has joined
	 */
	public Iterator<Channel> userJoinedChannels(final User user) {
		final List<Channel> joinedChannels = new Vector<Channel>();
		final Iterator<Channel> channels = channels();
		while(channels.hasNext()) {
			final Channel channel = channels.next();
			if (channel.hasJoined(user)) {
				joinedChannels.add(channel);
			}
		}
		return joinedChannels.iterator();
	}
	
	/**
	 * Returns a channel identified by its name
	 * @param name The name of the channel
	 * @return The channel or null, if channel was not found
	 */
	public Channel getChannel(final String name) {
		if (channelMap.containsKey(name)) {
			return channelMap.get(name);
		}
		return null;
	}
}
