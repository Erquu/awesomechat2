package awesomechat.util.channelmanagement;

public class UserAlreadyJoinedException extends ChannelException {
	private static final long serialVersionUID = 635319916432734650L;

	public UserAlreadyJoinedException(final String message) {
		super(message);
	}

}
