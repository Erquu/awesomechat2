package awesomechat.util.channelmanagement;

import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import awesomechat.util.usermanagement.User;

/**
 * Represents a Chat-Channel<br />
 * A channel is used to multicast messages to other chat-users in the same channel
 * @author Erquu
 */
public class Channel {
	/**
	 * The name of the channel<br />
	 * It is used to identify and join/leave it
	 */
	private String channelName;
	/**
	 * The password of the channel<br />
	 * If null, the channel has no password and can be accessed by anyone
	 */
	private String channelPassMD5;
	/**
	 * The welcome message of the channel<br />
	 * It will be sent to everyone joining this channel
	 */
	private String welcomeMessage = "Welcome in the channel %s!";
	/**
	 * The User who created the channel
	 */
	private User creator;
	/**
	 * Allow messaging in this channel<br />
	 * Disallowing is useful for channels like foyers
	 */
	private boolean allowMessaging = true;
	/**
	 * A list of all joined users
	 */
	private final List<User> joined = new Vector<User>();
	
	/**
	 * Creates a new Channel
	 * @param channelName The name of the channel<br />It is used to identify and join/leave it
	 * @param channelPassMD5 The password of the channel<br />If null, the channel has no password and can be accessed by anyone
	 * @param creator The User who created the channel 
	 * @param addUser Set to true if the creator should directly join the channel
	 */
	public Channel(final String channelName, final String channelPassMD5, final User creator, final boolean addUser) {
		this.channelName = channelName;
		this.channelPassMD5 = channelPassMD5;
		this.creator = creator;
		this.welcomeMessage = String.format(this.welcomeMessage, channelName);
		
		if (addUser) {
			joined.add(this.creator);
		}
	}
	/**
	 * Creates a new Channel
	 * @param channelName The name of the channel<br />It is used to identify and join/leave it
	 * @param channelPassMD5 The password of the channel<br />If null, the channel has no password and can be accessed by anyone
	 * @param creator The User who created the channel 
	 * @param addUser Set to true if the creator should directly join the channel
	 * @param allowMessaging Allow messaging in this channel
	 */
	public Channel(final String channelName, final String channelPassMD5, final User creator, final boolean addUser, final boolean allowMessaging) {
		this(channelName, channelPassMD5, creator, addUser);
		this.allowMessaging = allowMessaging;
	}
	
	/**
	 * Sets the welcome message to the given value
	 * @param welcomeMessage The new welcome message for this channel
	 */
	public void setWelcomeMessage(final String welcomeMessage) {
		this.welcomeMessage = welcomeMessage;
	}
	
	/**
	 * Returns the welcome message for this channel
	 * @return The welcome message for this channel
	 */
	public String getWelcomeMessage() {
		return welcomeMessage;
	}
	
	/**
	 * Returns the name of the channel
	 * @return The name of the channel
	 */
	public String getChannelName() {
		return channelName;
	}
	
	/**
	 * Returns the creator of the channel
	 * @return The creator of the channel
	 */
	public User getChannelCreator() {
		return creator;
	}
	
	/**
	 * Returns true if messaging is allowed in this channel
	 * @return true if messaging is allowed in this channel
	 */
	public boolean isMessagingAllowed() {
		return allowMessaging;
	}
	
	/**
	 * Returns an iterator of all users who are currently joined
	 * @return An iterator of all users who are currently joined
	 */
	public Iterator<User> joinedUsers() {
		return joined.iterator();
	}
	
	/**
	 * Checks if a user has joined the channel
	 * @param user The user to be checked
	 * @return true if user has joined the channel
	 */
	public boolean hasJoined(User user) {
		return joined.contains(user);
	}
	
	/**
	 * Adds a given user to the channel
	 * @param user The user to be added to the channel
	 */
	public void addUser(final User user) {
		if (!joined.contains(user)) {
			joined.add(user);
		}
	}
	
	/**
	 * Adds a User to the channel if the password matches
	 * @param user The user who wants to join
	 * @param channelPassMD5Check The password of the channel
	 * @throws UserAlreadyJoinedException The user has already joined the channel (and is still in it)
	 * @throws ChannelAuthenticationException The password authentication failed
	 */
	public void join(final User user, final String channelPassMD5Check) throws UserAlreadyJoinedException, ChannelAuthenticationException {
		boolean authenticationSucceeded = false;
		if (channelPassMD5 == null || (channelPassMD5Check != null && (authenticationSucceeded = channelPassMD5Check.equals(channelPassMD5)))) {
			if (!joined.contains(user)) {
				joined.add(user);
			} else {
				throw new UserAlreadyJoinedException("The user has already joined the channel.");
			}
		} else {
			if (!authenticationSucceeded) {
				throw new ChannelAuthenticationException("Wrong password for this channel!");
			}
		}
	}
	
	/**
	 * Replaces a user by another user
	 * @param oldUser the user to be replaced
	 * @param newUser the replacing user
	 */
	public void replace(final User oldUser, final User newUser) {
		if (joined.contains(oldUser)) {
			joined.remove(oldUser);
		}
		if (!joined.contains(newUser)) {
			joined.add(newUser);
		}
	}
	
	/**
	 * Removes a user from the channel
	 * @param user The user to be removed
	 */
	public void leave(final User user) {
		if (joined.contains(user)) {
			joined.remove(user);
		}
	}
	
	@Override
	public String toString() {
		return String.format("Channel[channelName=%s,joinedUserCount=%s]", channelName, joined.size());
	}
}
