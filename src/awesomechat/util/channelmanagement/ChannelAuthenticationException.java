package awesomechat.util.channelmanagement;

public class ChannelAuthenticationException extends ChannelException {
	private static final long serialVersionUID = -354488337633814333L;

	public ChannelAuthenticationException(final String message) {
		super(message);
	}
}
