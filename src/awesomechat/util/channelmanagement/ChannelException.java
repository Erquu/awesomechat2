package awesomechat.util.channelmanagement;

public class ChannelException extends Exception {
	private static final long serialVersionUID = -3541140823569792274L;

	public ChannelException(final String message) {
		super(message);
	}
}
