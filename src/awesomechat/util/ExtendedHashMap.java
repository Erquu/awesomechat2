package awesomechat.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

public class ExtendedHashMap<K, V> extends HashMap<K, V> {
	private static final long serialVersionUID = 466008048459962600L;

	/**
	 * Finds a key by a given value, if the value is in the Map
	 * @param object The value to search the key of
	 * @return The found key or null
	 */
	public K findKey(final V object) {
		final Iterator<K> keys = keySet().iterator();
		final Iterator<V> values = values().iterator();
		while (keys.hasNext() && values.hasNext()) {
			final K key = keys.next();
			final V value = values.next();
			
			if (value == object) {
				return key;
			}
		}
		return null;
	}
	
	/**
	 * Returns an iterator over all values from an array of given keys
	 * @param keys An array of key objects
	 * @return An iterator over all values from the given keys
	 */
	public Iterator<V> values(final K[] keys) {
		final List<V> values = new Vector<V>();
		for (final K key : keys) {
			if (containsKey(key)) {
				values.add(get(key));
			}
		}
		return values.iterator();
	}
}
