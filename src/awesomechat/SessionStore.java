package awesomechat;

import java.util.HashMap;
import java.util.Map;

/**
 * Stores Objects for the current session.<br />
 * The Objects in the store are identified by their respective class.
 * 
 * @author Erquu
 */
public final class SessionStore {
	private SessionStore() {}
	
	/** The object store */
	private static final Map<Class<? extends Object>, Object> store = new HashMap<Class<? extends Object>, Object>();
	
	/**
	 * Puts an element in the store.<br />
	 * If the store already contains an object of this type, it will be overwritten.
	 * @param key The type of the value element
	 * @param value The Object to store
	 * @return The stored object
	 */
	public static <T extends Object> T put(final Class<T> key, final T value) {
		store.put(key, value);
		return value;
	}
	
	/**
	 * Returns a stored object
	 * @param key The key or type of the object
	 * @return The object or null if not found
	 */
	public static <T extends Object> T get(final Class<T> key) {
		if (!store.containsKey(key))
			return null;
		return key.cast(store.get(key));
	}
}
