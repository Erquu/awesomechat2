package awesomechat.net.lo.server;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

import logging.Logger;
import awesomechat.DaemonThread;
import awesomechat.net.Packet;
import awesomechat.net.lo.AsyncReceive;
import awesomechat.net.lo.Connection;
import awesomechat.net.lo.callback.IAcceptCallback;
import awesomechat.net.lo.callback.IAsyncCallback;
import awesomechat.net.lo.callback.IReceiveCallback;

/**
 * TCP Server with asynchronous socket handling
 * 
 * @author Erquu
 */
public class AsyncTcpServer extends TcpServer {
	/** Indicating if accepting thread is running */
	private volatile boolean accepting = false;
	/** Indicating if receiving thread is running */
	private volatile boolean receiving = false;
	/** The thread for accepting clients */
	private AsyncConnectionAccept acceptingClass;
	/** The default logger */
	private final Logger logger = Logger.getLogger("default");
//	/** The thread for receiving messages */
//	private Thread receiveThread;
	
	/**
	 * Starts accepting incoming connections and receiving from them
	 * @param acceptCallback  Callback if socket was accepted
	 * @param receiveCallback Callback if message was received
	 * @param disconnectCallback Callback if a socket disconnects
	 */
	public void beginAcceptAndReceive(final IAcceptCallback<Connection> acceptCallback, final IReceiveCallback<Connection> receiveCallback, final IAsyncCallback<Connection> disconnectCallback) {
		beginAccept(new IAcceptCallback<Connection>() {
			@Override
			public void clientAccepted(final Connection acceptedConnection) {
				logger.debug("Client accepted, begin receiving");
				acceptCallback.clientAccepted(acceptedConnection);
				beginReceive(receiveCallback, disconnectCallback, acceptedConnection);
			}
		});
	}
	
	/**
	 * Starts accepting incoming connections
	 * @param accpetCallback Callback if connection established
	 * @throws TcpServerException Thrown if Server is already accepting incoming connections
	 */
	public void beginAccept(final IAcceptCallback<Connection> acceptCallback) {
		if (acceptingClass != null)
			return;
		acceptingClass = new AsyncConnectionAccept(this, acceptCallback);
		acceptingClass.start();
	}
	
	/**
	 * Stops accepting client
	 */
	public void endAccept() {
		Logger.getLogger("default").info("Stopping to accept clients");
	}
	
	/**
	 * Starts receiving from a Socket
	 * @param receiveCallback Callback if message was received
	 * @param receivingFrom The Socket to receive from
	 */
	public void beginReceive(final IReceiveCallback<Connection> receiveCallback, final IAsyncCallback<Connection> disconnectCallback, final Connection receivingFrom) {
		logger.info("Start receiving from %s asynchronously", receivingFrom);
		final AsyncReceive receiver = new AsyncReceive(receivingFrom, receiveCallback, disconnectCallback);
		receiver.start();
	}
	
	/**
	 * Stops receiving from all connected sockets
	 */
	public void endReceive() {
//		if (receiveThread != null) {
//			Logger.getLogger("default").info("Stopping to receive messages");
//			receiving = false;
//		}
	}
}
