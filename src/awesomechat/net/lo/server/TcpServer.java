package awesomechat.net.lo.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import logging.Logger;
import awesomechat.SessionStore;
import awesomechat.io.StreamHelper;
import awesomechat.net.Packet;

/**
 * Class for accepting remote connections and receiving {@link Packet} from the client
 * 
 * @author Erquu
 */
public class TcpServer {
	/** The socket where the clients connect to */
	protected ServerSocket serverSocket;
	/** The size of the buffer for receiving messages */
	protected int receiveBufferSize = 1024; // 1024
	/** StreamHelper instance to get the correct streams for a socket */
	protected final StreamHelper streamHelper = SessionStore.get(StreamHelper.class);
	
	/**
	 * Starts the server
	 * @param port The port to listen on
	 * @throws IOException Server creation failed
	 */
	public void start(final int port) throws IOException {
		serverSocket = createServerSocket(port);

		try {
			serverSocket.setReceiveBufferSize(receiveBufferSize);
		} catch (IOException e) {
			receiveBufferSize = serverSocket.getReceiveBufferSize();
			Logger.getLogger("default").warn("Could not set receive buffer size, using default value: %s", receiveBufferSize);
		}
		
		Logger.getLogger("default").info("Started server on port %s", port);
	}
	
	/**
	 * Stops the server
	 * @throws IOException Stopping failed
	 */
	public void stop() throws IOException {
		Logger.getLogger("default").info("Stopping server");
		serverSocket.close();
		Logger.getLogger("default").info("Stopped server successfully");
	}
	
	/**
	 * {@inheritDoc ServerSocket#getLocalPort()}
	 */
	public int getLocalPort() {
		return serverSocket.getLocalPort();
	}
	
	/**
	 * {@inheritDoc ServerSocket#accept()}
	 */
	public Socket accept() throws IOException {
		Logger.getLogger("default").debug("Waiting for socket accept, blocking Thread %s", Thread.currentThread().getName());
		final Socket clientSocket = serverSocket.accept();
		streamHelper.register(clientSocket);
		return clientSocket;
	}
	
	/**
	 * {@inheritDoc ServerSocket#isClosed()}
	 */
	public boolean isClosed() {
		return serverSocket.isClosed();
	}
	
	/** 
	 * Sends a {@link Packet} to a remote client
	 * @param socket The socket (client) to send the packet to
	 * @param packet The packet to be sent
	 * @throws IOException
	 */
	public void send(final Socket socket, final Packet packet) throws IOException {
		Logger.getLogger("default").debug("Sending Packet to %s", socket);
		if (!socket.isOutputShutdown()) {
			streamHelper.getOutputStream(socket).write(packet.getBuffer(), packet.getOffset(), packet.getLength());
		}
	}
	
	/**
	 * Sends a {@link Packet} to a set of remote clients
	 * @param sockets The sockets to send the packet to
	 * @param packet The packet to be sent
	 */
	public void multicast(final Socket[] sockets, final Packet packet) {
		for (final Socket socket : sockets) {
			try {
				send(socket, packet);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Creates a new ServerSocket
	 * @param port The port for the server to listen on
	 * @return The created ServerSocket
	 * @throws IOException ServerSocket creation failed
	 */
	protected ServerSocket createServerSocket(final int port) throws IOException {
		return new ServerSocket(port);
	}
}
