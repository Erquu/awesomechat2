package awesomechat.net.lo.server;

import java.io.IOException;
import java.net.Socket;

import logging.Logger;
import awesomechat.DaemonThread;
import awesomechat.net.lo.Connection;
import awesomechat.net.lo.callback.IAcceptCallback;

public class AsyncConnectionAccept implements Runnable {
	private final TcpServer server;
	private final IAcceptCallback<Connection> acceptCallback;
	private final Thread acceptThread;
	private boolean accepting = false;
	
	public AsyncConnectionAccept(final TcpServer server, final IAcceptCallback<Connection> acceptCallback) {
		this.server = server;
		this.acceptCallback = acceptCallback;
		
		acceptThread = new DaemonThread(this);
		acceptThread.setName("Connection Accept Thread");
	}
	
	public void start() {
		accepting = true;
		acceptThread.run();
	}
	
	public void stop() {
		accepting = false;
		try {
			acceptThread.join(100); // TODO: Make configurable
		} catch (InterruptedException e) {
			e.printStackTrace();
			acceptThread.interrupt();
		}
	}
	
	@Override
	public void run() {
		while (accepting) {
			Logger.getLogger("default").info("Waiting to accept new client");
			try {
				final Socket clientSocket = server.accept();
				Logger.getLogger("default").info("Client accepted: %s", clientSocket);
				acceptCallback.clientAccepted(new Connection(clientSocket));
			} catch (IOException e) {
				accepting = false;
				e.printStackTrace();
			}
		}
	}

}
