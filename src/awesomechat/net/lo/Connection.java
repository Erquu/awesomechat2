package awesomechat.net.lo;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketException;

import logging.Logger;
import awesomechat.SessionStore;
import awesomechat.io.StreamHelper;
import awesomechat.net.Packet;
import awesomechat.net.lo.callback.IAsyncCallback;

public class Connection {
	private static final Logger LOGGER = Logger.getLogger("default");
	
	private Socket socket;
	
	private IAsyncCallback<Connection> disconnectCallback = null;
	
	protected InputStream socketIn;
	protected OutputStream socketOut;
	protected int receiveBufferSize; 
	
	public Connection(final Socket socket) throws SocketException {
		this(socket, null);
	}
	public Connection(final Socket socket, final IAsyncCallback<Connection> disconnectCallback) throws SocketException {
		if (socket == null)
			throw new NullPointerException(); // Maybe some argument exception
		
		this.disconnectCallback = disconnectCallback;
		setSocket(socket);
	}
	
	public void close() {
		if (isConnected()) {
			try {
				socketIn.close();
				socket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (disconnectCallback != null) {
				disconnectCallback.eventTriggered(this, this);
			}
		}
	}
	
	public void setDisconnectCallback(final IAsyncCallback<Connection> disconnectCallback) {
		this.disconnectCallback = disconnectCallback;
	}
	
	public Socket getSocket() {
		return socket;
	}
	
	public boolean isConnected() {
		return !(socket.isInputShutdown() && socket.isClosed());
	}
	
	public InputStream getInputStream() {
		return socketIn;
	}
	
	public OutputStream getOutputStream() {
		return socketOut;
	}
	
	public void setSocket(final Socket socket) throws SocketException {
		this.socket = socket;
		
		final StreamHelper streamHelper = SessionStore.get(StreamHelper.class);
		socketOut = streamHelper.getOutputStream(socket);
		
		receiveBufferSize = socket.getReceiveBufferSize();
		socketIn = streamHelper.getInputStream(socket);
	}
	
	public void send(final Packet packet) throws IOException {
		Logger.getLogger("default").debug("Sending Packet to %s", socket);
		
		if (!socket.isOutputShutdown()) {
			socketOut.write(packet.getBuffer(), packet.getOffset(), packet.getLength());
		}
	}
	
	public Packet receive() throws IOException {
		final byte[] buffer = new byte[receiveBufferSize];
		final int receiveByteCount = socketIn.read(buffer, 0, receiveBufferSize);
		
		LOGGER.debug("Received Packet from %s", socket);
		LOGGER.debug("  - rbs:%s rbc:%s", receiveBufferSize, receiveByteCount);
		
		if (receiveByteCount >= 0) {
			return new Packet(buffer, 0, receiveByteCount);
		} else {
			LOGGER.info("Client socket closed: %s", socket.isClosed());
			//disconnectCallback.eventTriggered(AsyncTcpServer.this, receivingFrom);
			socketIn.close();
			socket.close();
			
			if (disconnectCallback != null) {
				disconnectCallback.eventTriggered(this, this);
			}
			
			return null;
		}
	}
}
