package awesomechat.net.lo;

import java.io.IOException;

import awesomechat.DaemonThread;
import awesomechat.net.Packet;
import awesomechat.net.lo.callback.IAsyncCallback;
import awesomechat.net.lo.callback.IReceiveCallback;

public class AsyncReceive implements Runnable {
	private final Connection connection;
	private final IReceiveCallback<Connection> receiveCallback;
	private final IAsyncCallback<Connection> disconnectCallback;
	private final Thread receiveThread;
	
	private boolean receiving = false;
	
	public AsyncReceive(final Connection connection, final IReceiveCallback<Connection> receiveCallback, final IAsyncCallback<Connection> disconnectCallback) {
		this.connection = connection;
		this.receiveCallback = receiveCallback;
		this.disconnectCallback = disconnectCallback;
		
		receiveThread = new DaemonThread(this);
		receiveThread.setName("Receive thread");
	}

	public boolean isReceiving() {
		return receiving;
	}
	
	public void start() {
		receiving = true;
	}
	
	public void stop() {
		receiving = false;
		try {
			receiveThread.join(100); // TODO: Make configurable
		} catch (InterruptedException e) {
			e.printStackTrace();
			receiveThread.interrupt();
		}
	}

	@Override
	public void run() {
		while (!connection.isConnected() && receiving) {
			Packet packet = null;
			try {
				packet = connection.receive();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			if (packet != null) {
				receiveCallback.packetReceived(connection, packet);
			} else {
				receiving = false;
				connection.close();
				disconnectCallback.eventTriggered(this, connection);
			}
		}
	}
}
