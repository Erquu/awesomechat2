package awesomechat.net.lo.client;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;

import logging.Logger;
import awesomechat.SessionStore;
import awesomechat.io.StreamHelper;
import awesomechat.net.Packet;
import awesomechat.net.TcpState;

/**
 * Class for connecting to a remote server and receiving message<br />
 * No further actions are done
 * 
 * @author Erquu
 */
public class TcpClient {
	/** The address of the remote Server */
	protected final InetAddress remoteAddress;
	/** The port of the remote server */
	private final int remotePort;
	/** Indicates if the connection close was planned (if it happens) */
	private boolean plannedClosing = false;
	/** The state of the tcp client */
	private TcpState clientState = TcpState.UNKNOWN;
	/** The socket holding the connection to the server */
	protected Socket clientSocket;
	/** The size of the buffer for receiving messages */
	protected int receiveBufferSize = 1024;
	/** The output stream to write to the server */
	protected OutputStream clientOutputStream;
	/** The input stream to receive from the server */
	protected InputStream clientInputStream;
	/** StreamHelper instance to get the correct streams for a socket */
	protected final StreamHelper streamHelper = SessionStore.get(StreamHelper.class);
	
	/**
	 * Creates a new TcpClient
	 * @param remoteAddress The (ip-)address of the remote server
	 * @param port The port of the remote server
	 */
	public TcpClient(final String remoteAddress, final int port) throws UnknownHostException {
		this(InetAddress.getByName(remoteAddress), port);
	}
	/**
	 * Creates a new TcpClient
	 * @param remoteAddress The address of the remote server
	 * @param port The port of the remote server
	 */
	public TcpClient(final InetAddress remoteAddress, final int port) {
		this.remoteAddress = remoteAddress;
		this.remotePort = port;
	}
	
	//
	// GETTER AND SETTER
	//
	
	/**
	 * Returns the Inet address of the remote server
	 * @return The Inet address of the remote server
	 */
	public InetAddress getRemoteAddress() {
		return remoteAddress;
	}
	
	/**
	 * Returns the port of the remote server
	 * @return The port of the remote server
	 */
	public int getRemotePort() {
		return remotePort;
	}
	
	/**
	 * Returns the state of the tcp client
	 * @return The state of the tcp client
	 */
	public TcpState getState() {
		return clientState;
	}
	
	/**
	 * Returns the socket holding the connection to the server
	 * @return The socket holding the connection to the server
	 */
	public Socket getSocket() {
		return clientSocket;
	}
	
	/**
	 * Returns if the client is connected to the remote Server
	 * @return true if client is connected to the remote Server
	 */
	public boolean isConnected() {
		return clientSocket.isConnected();
	}
	
	//
	// EVENTS
	//
	
	/**
	 * Called if the connection was closed
	 * @param planned false if the connection closed abnormally
	 */
	public void serverDisconnected(final boolean planned) {
		Logger.getLogger("default").warn("Server disconnected");
	}
	
	///
	/// SOCKET ACTIONS
	///
	
	/**
	 * Starts connecting to the remote server
	 * @throws IOException Socket could not be created
	 */
	public void connect() throws IOException {
		if (clientSocket != null) {
			streamHelper.unregister(clientSocket);
			close();
		}
		
		clientSocket = createSocket();
		
		try {
			clientSocket.setReceiveBufferSize(receiveBufferSize);
		} catch (IOException e) {
			receiveBufferSize = clientSocket.getReceiveBufferSize();
		}
		
		streamHelper.register(clientSocket);
		clientOutputStream = streamHelper.getOutputStream(clientSocket);
		clientInputStream = streamHelper.getInputStream(clientSocket);

		clientState = TcpState.ESTABLISHED;
		
		Logger.getLogger("default").info("Connected");
	}
	
	/**
	 * Creates a Socket holding the connection to the server
	 * @throws IOException Socket could not be created
	 */
	protected Socket createSocket() throws IOException {
		return new Socket(this.remoteAddress, this.remotePort);
	}
	
	/**
	 * Sends a packet to the server socket
	 * @param packet The packet to be sent
	 * @throws Exception 
	 */
	public void send(final Packet packet) {
		try {
			clientOutputStream.write(packet.getBuffer());
			clientOutputStream.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Waits for receiving a packet from the server socket<br />
	 * <b>This method is blocking</b>
	 * @return The received packet or null if connection broke
	 * @throws IOException 
	 */
	public Packet receive() {
		final byte[] inBuffer = new byte[receiveBufferSize];
		int receivedMessageSize = -1;
		try {
			receivedMessageSize = clientInputStream.read(inBuffer, 0, receiveBufferSize);
		} catch (final SocketException e) {
			clientState = TcpState.CLOSED;
			serverDisconnected(plannedClosing);
		} catch (final IOException ex) {
			ex.printStackTrace();
		}
		if (receivedMessageSize == -1)
			return null;
			
		final byte[] outBuffer = new byte[receivedMessageSize];
		System.arraycopy(inBuffer, 0, outBuffer, 0, receivedMessageSize);
		
		return new Packet(outBuffer);
	}
	
	/**
	 * Closes the connection to the remote server
	 * @throws IOException Connection could not be closed
	 */
	public void close() throws IOException {
		plannedClosing = true;
		
		clientState = TcpState.CLOSING;
		clientInputStream.close();
		clientOutputStream.close();
		clientSocket.close();
		clientState = TcpState.CLOSED;
	}
}
