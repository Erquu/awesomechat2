package awesomechat.net.lo.client;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import logging.Logger;
import awesomechat.DaemonThread;
import awesomechat.net.Packet;
import awesomechat.net.lo.AsyncReceive;
import awesomechat.net.lo.callback.IAsyncCallback;
import awesomechat.net.lo.callback.IReceiveCallback;
import awesomechat.net.messaging.Message;

/**
 * Tcp Client for doing asynchronous work
 * 
 * @author Erquu
 */
public class AsyncTcpClient extends TcpClient {
	/** Indicating if receiving thread is running */
	private volatile boolean receiving = false;
	/** The thread, receiving packets */
	private AsyncReceive receiver = null;

	/**
	 * Creates a new asynchronous Tcp client
	 * @param remoteAddress The address of the remote server 
	 * @param port The port of the remote server
	 * @throws UnknownHostException Host could not be found
	 */
	public AsyncTcpClient(final String remoteAddress, final int port) throws UnknownHostException {
		super(remoteAddress, port);
	}
	/**
	 * Creates a new asynchronous Tcp client
	 * @param remoteAddress The address of the remote server 
	 * @param port The port of the remote server
	 */
	public AsyncTcpClient(final InetAddress remoteAddress, final int port) {
		super(remoteAddress, port);
	}
	
	/**
	 * Returns if the client is listening for incoming messages
	 * @return true if the client is listening for incoming messages
	 */
	public boolean isReceiving() {
		return receiving;
	}
	
	@Override
	public void serverDisconnected(boolean planned) {
		endReceive();
		
		super.serverDisconnected(planned);
	}
	
	/**
	 * Sends a packet asynchronously to the server socket
	 * @param packet The packet to be sent
	 * @param failCallback Callback to handle exception if sending failed, can be null
	 */
	public void sendAsync(final Packet packet, final IAsyncCallback<Exception> failCallback) {
		final Thread sendingThread = new DaemonThread(new Runnable() {
			@Override public void run() {
				try {
					send(packet);
				} catch (final Exception e) {
					e.printStackTrace();
					if (failCallback != null) {
						failCallback.eventTriggered(AsyncTcpClient.this, e);
					}
				}
			}
		});
		sendingThread.setName("Packetsender");
		sendingThread.start();
	}
	
	/**
	 * Connects asynchronously to the server
	 * @param successCallback Callback if connection is established
	 * @param failCallback Callback if connecting failed
	 */
	public void beginConnect(final IAsyncCallback<AsyncTcpClient> successCallback, final IAsyncCallback<IOException> failCallback) {
		final Thread connectThread = new DaemonThread(new Runnable() {
			@Override
			public void run() {
				try {
					Logger.getLogger("default").info("Connecting to server");
					connect();
					Logger.getLogger("default").info("Connected");
					if (successCallback != null) {
						successCallback.eventTriggered(AsyncTcpClient.this, AsyncTcpClient.this);
					}
				} catch (IOException e) {
					if (failCallback != null) {
						failCallback.eventTriggered(this, e);
					}
				}
			}
		});
		connectThread.setName("ClientConnectThread");
		connectThread.start();
	}
	
	/**
	 * Starts receiving from the server socket
	 * @param receiveCallback Callback for received packets
	 * @throws TcpClientException Throws if client is already receiving asynchronously
	 */
	public void beginReceive(final IReceiveCallback<Connection> receiveCallback) {
		if (receiver != null)
			return;
		receiver = new AsyncReceive(connection, receiveCallback, disconnectCallback);
	}
	
	/**
	 * Stops receiving from the server socket
	 */
	public void endReceive() {
		if (receiveThread != null) {
			try {
				receiving = false;
				receiveThread.join(20);
			} catch (InterruptedException e) {
				receiveThread.interrupt();
			}
		}
	}
}
