package awesomechat.net.lo.callback;

/**
 * Interface for callbacks from asynchronous operations
 *  
 * @author Erquu
 *
 * @param <T> The requested type for the callback argument
 */
public interface IAsyncCallback<T> {
	/**
	 * Asynchronous operation triggered the callback
	 * @param sender The sender of the callback
	 * @param arg The argument from the operation
	 */
	void eventTriggered(final Object sender, final T arg);
}
