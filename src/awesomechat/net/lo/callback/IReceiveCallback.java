package awesomechat.net.lo.callback;

import java.net.Socket;

import javax.net.ssl.SSLSocket;

import awesomechat.net.Packet;
import awesomechat.net.lo.Connection;

/**
 * Callback interface for received Packets from a remote Socket
 * @author Erquu
 *
 * @param <T> The Type of Socket ({@link SSLSocket}/{@link Socket})
 */
public interface IReceiveCallback<T extends Connection> {
	void packetReceived(final T sender, final Packet packet);
}
