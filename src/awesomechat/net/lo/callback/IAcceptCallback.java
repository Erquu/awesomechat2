package awesomechat.net.lo.callback;

import java.net.Socket;

import javax.net.ssl.SSLSocket;

import awesomechat.net.lo.Connection;

/**
 * Callback interface for accepted client sockets
 * @author Erquu
 *
 * @param <T> The Type of Socket ({@link SSLSocket}/{@link Socket})
 */
public interface IAcceptCallback<T extends Connection> {
	void clientAccepted(final T acceptedConnection);
}
