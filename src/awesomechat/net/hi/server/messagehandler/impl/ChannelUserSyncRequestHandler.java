package awesomechat.net.hi.server.messagehandler.impl;

import java.net.Socket;
import java.util.Iterator;

import awesomechat.SessionStore;
import awesomechat.io.json.JSONArray;
import awesomechat.io.json.JSONObject;
import awesomechat.net.hi.server.messagehandler.IMessageHandler;
import awesomechat.net.messaging.Message;
import awesomechat.net.messaging.MessageType;
import awesomechat.util.channelmanagement.Channel;
import awesomechat.util.channelmanagement.ChannelManager;
import awesomechat.util.usermanagement.User;
import awesomechat.util.usermanagement.UserManager;

public class ChannelUserSyncRequestHandler implements IMessageHandler {
	/**
	 * Check out {@link MessageType#CHANNELUSERSYNCREQUEST} and {@link MessageType#CHANNELUSERSYNCRESPONSE}
	 */
	@Override
	public Message process(JSONObject arg, final User senderUser, final Socket sender) {
		
		final UserManager userManager = SessionStore.get(UserManager.class);
		final ChannelManager channelManager = SessionStore.get(ChannelManager.class);
		
		final JSONObject message = arg.get("Message");
		
		final String userID = senderUser.getUserID();
		final String channelName = message.get("channelName").toString();
		
		boolean success = false;
		String responseMessage = null;
		
		final User user = userManager.getUser(userID);
		final Channel channel = channelManager.getChannel(channelName);
		
		final JSONArray userArray = new JSONArray();
		
		if (channel != null) {
			if (channel.hasJoined(user)) {
				success = true;
				final Iterator<User> usersInChannel = channel.joinedUsers();
				while (usersInChannel.hasNext()) {
					final User userInChannel = usersInChannel.next();
					if (!userInChannel.equals(user)) {
						userArray.add(
							(new JSONObject())
								.add("userID", userInChannel.getUserID())
								.add("displayName", userInChannel.getDisplayName())
								.add("state", userInChannel.getUserState().name()));
					}
				}
			} else {
				responseMessage = "You are not allowed to synchronize on this channel!";
			}
		} else {
			responseMessage = "The channel does not exist.";
		}

		final JSONObject responseObject = (new JSONObject()).add("Message", (
			new JSONObject())
				.add("userID", userID)
				.add("channelName", channelName)
				.add("success", success)
				.add("message", responseMessage)
				.add("users", userArray));
		
		return new Message(MessageType.CHANNELUSERSYNCRESPONSE, responseObject);
	}
}
