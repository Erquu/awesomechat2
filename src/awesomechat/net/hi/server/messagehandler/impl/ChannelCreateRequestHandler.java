package awesomechat.net.hi.server.messagehandler.impl;

import java.net.Socket;

import logging.Logger;
import awesomechat.SessionStore;
import awesomechat.io.json.JSONObject;
import awesomechat.net.hi.server.messagehandler.IMessageHandler;
import awesomechat.net.messaging.Message;
import awesomechat.net.messaging.MessageType;
import awesomechat.util.channelmanagement.Channel;
import awesomechat.util.channelmanagement.ChannelManager;
import awesomechat.util.usermanagement.User;
import awesomechat.util.usermanagement.UserManager;

public class ChannelCreateRequestHandler implements IMessageHandler {
	/**
	 * Check out {@link MessageType#CHANNELCREATEREQUEST} and {@link MessageType#CHANNELCREATERESPONSE}
	 */
	@Override
	public Message process(final JSONObject arg, final User senderUser, final Socket sender) {
		final Logger logger = Logger.getLogger("default");
		
		final JSONObject message = arg.get("Message");
		
		final String userID = senderUser.getUserID();
		final String channelName = message.getString("channelName");
		final String passMD5 = message.getString("password");
		final String welcomeMessage = message.getString("welcome");
		
		final UserManager userManager = SessionStore.get(UserManager.class);
		final ChannelManager channelManager = SessionStore.get(ChannelManager.class);
		
		boolean success = false;
		String responseMessage = null;
		
		logger.info("Creating channel");
		final Channel channel = channelManager.createChannel(channelName, passMD5, userManager.getUser(userID), true);
		if (channel != null) {
			logger.info("Channel created: %s", channel);
			success = true;
			if (!welcomeMessage.trim().isEmpty()) {
				channel.setWelcomeMessage(welcomeMessage);
			}
			//responseMessage = channel.getWelcomeMessage();
		} else {
			responseMessage = "The channel already exists.";
			logger.warn(responseMessage);
		}
		
		final JSONObject responseObject = (new JSONObject()).add("Message", (
			new JSONObject())
				.add("userID", userID)
				.add("channelName", channelName)
				.add("success", success)
				.add("message", responseMessage));
		
		return new Message(MessageType.CHANNELCREATERESPONSE, responseObject);
	}
}
