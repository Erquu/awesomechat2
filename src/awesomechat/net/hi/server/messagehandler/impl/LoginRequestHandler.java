package awesomechat.net.hi.server.messagehandler.impl;

import java.net.Socket;
import java.util.Iterator;

import awesomechat.SessionStore;
import awesomechat.io.json.JSONObject;
import awesomechat.net.hi.server.messagehandler.IMessageHandler;
import awesomechat.net.messaging.Message;
import awesomechat.net.messaging.MessageType;
import awesomechat.util.ExtendedHashMap;
import awesomechat.util.channelmanagement.Channel;
import awesomechat.util.channelmanagement.ChannelManager;
import awesomechat.util.usermanagement.User;
import awesomechat.util.usermanagement.UserException;
import awesomechat.util.usermanagement.UserManager;
import awesomechat.util.usermanagement.UserSocketMap;
import awesomechat.util.usermanagement.UserState;

public class LoginRequestHandler implements IMessageHandler {
	/**
	 * Check out {@link MessageType#LOGINREQUEST} and {@link MessageType#LOGINRESPONSE} 
	 */
	@Override
	public Message process(final JSONObject arg, final User senderUser, final Socket sender) {
		
		final ChannelManager channelManager = SessionStore.get(ChannelManager.class);
		final UserManager userManager = SessionStore.get(UserManager.class);
		@SuppressWarnings("unchecked")
		final UserSocketMap userSocketMap = SessionStore.get(UserSocketMap.class);
		
		final JSONObject message = arg.get("Message");
		
		final String oldUserID = senderUser.getUserID();
		final String newUserID = message.getString("newUserID");
		final String passMD5   = message.getString("password");
		
		boolean success = false;
		String responseMessage = "";
		User user = null;
		
		try {
			user = userManager.loginUser(oldUserID, newUserID, passMD5);
			
			if (user.getUserState() == UserState.OFFLINE) {
				user.setUserState(UserState.ONLINE);
			}
			
			userSocketMap.remove(senderUser);
			userSocketMap.put(user, sender);

			final Iterator<Channel> oldChannels = channelManager.userJoinedChannels(senderUser);
			while (oldChannels.hasNext()) {
				oldChannels.next().replace(senderUser, user);
			}
			
			success = true;
		} catch (UserException e) {
			responseMessage = e.getMessage();
		}
		
		final JSONObject data = new JSONObject()
			.add("userID", success?newUserID:oldUserID)
			.add("oldUserID", oldUserID)
			.add("success", success)
			.add("message", responseMessage);
		
		if (success && user != null) {
			data.add("userData", (new JSONObject()).add("displayName", user.getDisplayName()));
		}
		
		final JSONObject object = (new JSONObject()).add("Message", data);
		
		return new Message(MessageType.LOGINRESPONSE, object);
	};
}
