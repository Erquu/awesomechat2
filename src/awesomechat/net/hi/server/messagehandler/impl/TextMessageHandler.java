package awesomechat.net.hi.server.messagehandler.impl;

import java.net.Socket;
import java.util.Iterator;

import awesomechat.SessionStore;
import awesomechat.io.json.JSONObject;
import awesomechat.net.hi.server.messagehandler.IMessageHandler;
import awesomechat.net.messaging.Message;
import awesomechat.net.messaging.MessageType;
import awesomechat.net.messaging.MulticastMessage;
import awesomechat.util.channelmanagement.Channel;
import awesomechat.util.channelmanagement.ChannelManager;
import awesomechat.util.usermanagement.User;
import awesomechat.util.usermanagement.UserState;

public class TextMessageHandler implements IMessageHandler {
	@Override
	public Message process(final JSONObject arg, final User senderUser, final Socket sender) {
		arg.get("Message").add("userID", senderUser.getUserID());
		
		final MulticastMessage response = new MulticastMessage();
		response.setMessageType(MessageType.MESSAGE);
		response.setMessageContent(arg);
		
		final String channelName = arg.getString("Message", "channelName");
		final ChannelManager channelManager = SessionStore.get(ChannelManager.class);
		
		final Channel channel = (channelName.length() == 0) ? channelManager.getDefaultChannel() : channelManager.getChannel(channelName);
		if (channel != null && channel.isMessagingAllowed()) {
			final Iterator<User> usersInChannel = channel.joinedUsers();
			while (usersInChannel.hasNext()) {
				final User user = usersInChannel.next();
				if (!user.equals(senderUser) && user.getUserState() != UserState.OFFLINE) {
					response.addToReceiveList(user);
				}
			}
		}
		
		return response;
	}
}
