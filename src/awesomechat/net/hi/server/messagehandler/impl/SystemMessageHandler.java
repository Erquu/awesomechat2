package awesomechat.net.hi.server.messagehandler.impl;

import java.net.Socket;

import logging.Logger;
import awesomechat.io.json.JSONObject;
import awesomechat.net.hi.server.messagehandler.IMessageHandler;
import awesomechat.net.messaging.Message;
import awesomechat.net.messaging.MessageType;
import awesomechat.util.usermanagement.User;

public class SystemMessageHandler implements IMessageHandler {
	/**
	 * Check out {@link MessageType#SYSTEM}
	 */
	@Override
	public Message process(final JSONObject arg, final User senderUser, final Socket sender) {
		final String type = arg.getString("type");
		
		if (type != null) {
			switch (type.toLowerCase().trim()) {
				case "block":
					try {
						long timeout = arg.getLong("timeout");
						Logger.getLogger("default").info("Sleeping %sms (%s)", timeout, Thread.currentThread().getName());
						Thread.sleep(timeout);
						Logger.getLogger("default").info("Woke up just now (%s)", Thread.currentThread().getName());
					} catch (final NumberFormatException e) {
					} catch (final InterruptedException e) {
					}
					break;
			}
		}
		
		return null;
	}

}
