package awesomechat.net.hi.server.messagehandler.impl;

import java.net.Socket;

import awesomechat.SessionStore;
import awesomechat.io.json.JSONObject;
import awesomechat.net.hi.server.messagehandler.IMessageHandler;
import awesomechat.net.messaging.Message;
import awesomechat.net.messaging.MessageType;
import awesomechat.util.channelmanagement.Channel;
import awesomechat.util.channelmanagement.ChannelManager;
import awesomechat.util.usermanagement.User;
import awesomechat.util.usermanagement.UserManager;

public class ChannelLeaveRequestHandler implements IMessageHandler {
	/**
	 * Check out {@link MessageType#CHANNELLEAVEREQUEST} and {@link MessageType#CHANNELLEAVERESPONSE}
	 */
	@Override
	public Message process(final JSONObject arg, final User senderUser, final Socket sender) {
		
		final UserManager userManager = SessionStore.get(UserManager.class);
		final ChannelManager channelManager = SessionStore.get(ChannelManager.class);
		
		final JSONObject message = arg.get("Message");
		
		final String userID = senderUser.getUserID();
		final String channelName = message.getString("channelName");
		
		boolean success = false;
		String responseMessage = null;
		
		final User user = userManager.getUser(userID);
		final Channel channel = channelManager.getChannel(channelName);
		
		if (channel != null) {
			if (channel.hasJoined(user)) {
				channel.leave(user);
				responseMessage = "You've left the message successfully.";
				success = true;
			} else {
				responseMessage = "You can't leave a channel you haven't joined.";
			}
		} else {
			responseMessage = "The channel does not exist.";
		}
		
		final JSONObject responseObject = (new JSONObject()).add("Message", (
			new JSONObject())
				.add("userID", userID)
				.add("channelName", channelName)
				.add("success", success)
				.add("message", responseMessage));
		
		return new Message(MessageType.CHANNELLEAVERESPONSE, responseObject);
	}
}
