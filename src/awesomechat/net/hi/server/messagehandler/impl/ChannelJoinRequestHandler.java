package awesomechat.net.hi.server.messagehandler.impl;

import java.net.Socket;

import awesomechat.SessionStore;
import awesomechat.io.json.JSONObject;
import awesomechat.net.hi.server.messagehandler.IMessageHandler;
import awesomechat.net.messaging.Message;
import awesomechat.net.messaging.MessageType;
import awesomechat.util.channelmanagement.Channel;
import awesomechat.util.channelmanagement.ChannelException;
import awesomechat.util.channelmanagement.ChannelManager;
import awesomechat.util.usermanagement.User;
import awesomechat.util.usermanagement.UserManager;

public class ChannelJoinRequestHandler implements IMessageHandler {
	/**
	 * Check out {@link MessageType#CHANNELJOINREQUEST} and {@link MessageType#CHANNELJOINRESPONSE}
	 */
	@Override
	public Message process(final JSONObject arg, final User senderUser, final Socket sender) {

		final UserManager userManager = SessionStore.get(UserManager.class);
		final ChannelManager channelManager = SessionStore.get(ChannelManager.class);
		
		final JSONObject message = arg.get("Message");
		
		final String userID = senderUser.getUserID();
		final String channelName = message.getString("channelName");
		final String passMD5 = message.getString("password");
		
		boolean success = false;
		String responseMessage = null;
		
		final Channel channel = channelManager.getChannel(channelName);
		if (channel != null) {
			final User user = userManager.getUser(userID);
			try {
				channel.join(user, passMD5);
				responseMessage = channel.getWelcomeMessage();
				success = true;
			} catch (ChannelException e) {
				responseMessage = e.getMessage();
			}
		} else {
			responseMessage = "The channel does not exist.";
		}

		final JSONObject responseObject = (new JSONObject()).add("Message", (
			new JSONObject())
				.add("userID", userID)
				.add("channelName", channelName)
				.add("success", success)
				.add("message", responseMessage));
		
		return new Message(MessageType.CHANNELJOINRESPONSE, responseObject);
	}
}
