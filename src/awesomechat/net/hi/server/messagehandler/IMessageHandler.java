package awesomechat.net.hi.server.messagehandler;

import java.net.Socket;

import awesomechat.io.json.JSONObject;
import awesomechat.net.messaging.Message;
import awesomechat.util.usermanagement.User;

public interface IMessageHandler {
	Message process(final JSONObject arg, final User senderUser, final Socket sender);
}
