package awesomechat.net.hi.server;

import java.net.Socket;

import awesomechat.net.hi.server.messagehandler.IMessageHandler;
import awesomechat.net.hi.server.messagehandler.impl.ChannelCreateRequestHandler;
import awesomechat.net.hi.server.messagehandler.impl.ChannelJoinRequestHandler;
import awesomechat.net.hi.server.messagehandler.impl.ChannelLeaveRequestHandler;
import awesomechat.net.hi.server.messagehandler.impl.ChannelUserSyncRequestHandler;
import awesomechat.net.hi.server.messagehandler.impl.LoginRequestHandler;
import awesomechat.net.hi.server.messagehandler.impl.SystemMessageHandler;
import awesomechat.net.hi.server.messagehandler.impl.TextMessageHandler;
import awesomechat.net.messaging.Message;
import awesomechat.util.usermanagement.User;

/**
 * Message processor for handling messages
 * 
 * @author Erquu
 */
public class MessageProcessor {
	/**
	 * Initialization is not allowed
	 */
	private MessageProcessor() {}
	
	/**
	 * Processes a message
	 * @param request The message to be processed
	 * @return The response for the processed message or null if no response is required
	 */
	@SuppressWarnings("incomplete-switch")
	public static Message process(final Message request, final User senderUser, final Socket sender) {
		Message response = null;
		
		IMessageHandler handler = null;
		switch (request.getMessageType()) {
			case LOGINREQUEST:			handler = new LoginRequestHandler(); break;
			case CHANNELCREATEREQUEST:	handler = new ChannelCreateRequestHandler(); break;
			case CHANNELJOINREQUEST:	handler = new ChannelJoinRequestHandler(); break;
			case CHANNELUSERSYNCREQUEST:handler = new ChannelUserSyncRequestHandler(); break;
			case CHANNELLEAVEREQUEST:	handler = new ChannelLeaveRequestHandler(); break;
			case MESSAGE:				handler = new TextMessageHandler(); break;
			case SYSTEM:				handler = new SystemMessageHandler(); break;
		}
		
		if (handler != null) {
			response = handler.process(request.getMessageContent(), senderUser, sender);
		}
		
		return response;
	}
}
