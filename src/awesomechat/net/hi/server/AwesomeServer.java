package awesomechat.net.hi.server;

import java.io.IOException;
import java.net.Socket;
import java.util.Iterator;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import logging.Logger;
import awesomechat.SessionStore;
import awesomechat.io.json.JSONObject;
import awesomechat.net.Packet;
import awesomechat.net.hi.sec.CipherHelper;
import awesomechat.net.hi.sec.ICipherSuite;
import awesomechat.net.hi.sec.impl.NoCipherSuite;
import awesomechat.net.hi.sec.impl.RSACipherSuite;
import awesomechat.net.lo.callback.IAcceptCallback;
import awesomechat.net.lo.callback.IAsyncCallback;
import awesomechat.net.lo.callback.IReceiveCallback;
import awesomechat.net.lo.server.AsyncTcpServer;
import awesomechat.net.messaging.Message;
import awesomechat.net.messaging.MessageType;
import awesomechat.net.messaging.MulticastMessage;
import awesomechat.util.ExtendedHashMap;
import awesomechat.util.channelmanagement.Channel;
import awesomechat.util.channelmanagement.ChannelManager;
import awesomechat.util.usermanagement.User;
import awesomechat.util.usermanagement.UserManager;
import awesomechat.util.usermanagement.UserSocketMap;
import awesomechat.util.usermanagement.UserState;

public class AwesomeServer extends AsyncTcpServer implements IAcceptCallback<Socket>, IReceiveCallback<Socket>, IAsyncCallback<Socket> {
	//public static final int DEFAULT_PORT = Short.MAX_VALUE * 2 - 1;
	/** The default Awesomechat port */
	public static final int DEFAULT_PORT = 42042;
	
	/**
	 * The number of minimum items in the queue before a new thread is started.<br />
	 * This will only be considered if there is too much load. */
	private static final int CARE_ABOUT_LOAD_QUEUE_SIZE = 5;
	/**
	 * The maximum increase of queue-items during processing of one element before starting a new thread.<br />
	 * Example (with CARE_ABOUT_LOAD_QUEUE_SIZE=5):
	 * <table border="1">
	 *   <tr><td>Elements before<br />processing</td><td>Elements after<br />processing is finished</td><td>Too much load/<br />Start new thread</td></tr>
	 *   <tr><td>0</td><td>3</td><td>FALSE</td></tr>
	 *   <tr><td>0</td><td>8</td><td>TRUE</td></tr>
	 * </table>
	 */
	private static final int MAX_LOAD_INCREASE = 5;
	/** The maximum timeout for processing a request before starting a new thread (in ms) */
	// In Productive usage, this should be as low as possible
	// Lower this value to get better queue processing performance, this may raise the load of your server (physical server)
	// Raise this value to lower the load of your server (physical), longer message transfer times could be the result
	private static final long QUEUE_TIMEOUT = 400;
	
	/** User Socket mapping for identification */
	private final UserSocketMap socketUserMap = new UserSocketMap();
	/** Reference to the global user manager */
	private final UserManager userManager = SessionStore.get(UserManager.class);
	/** Reference to the global channel manager */
	private final ChannelManager channelManager = SessionStore.get(ChannelManager.class);
	/** Indicates if the server is running */
	private boolean isRunning = false;
	/** Queue with messages to process */
	private final Queue<PriorityQueueElement> messageQueue = new PriorityQueue<PriorityQueueElement>();
	/** ExecutorService to enable multicore processing */
	private final ExecutorService queueProcessingService = Executors.newCachedThreadPool();
	/** Number of simultaneous running Threads */
	private volatile int runningThreads = 0;
	/** Timestamp of the last time the queue started processing an element */
	private volatile long lastQueueWork = 0;
	/** Indicator showing if there is too much load and another Thread needs to be started */
	private volatile boolean tooMuchLoad = false;
	/** The ICipherSuite used for encryption and decryption client to server messages */
	private ICipherSuite cipherSuite;
	/** The ICipherSuites mapped to the client sockets for encrypting and decrypting from client messages */
	private final ExtendedHashMap<Socket, ICipherSuite> socketCipherMap = new ExtendedHashMap<Socket, ICipherSuite>();
	
	public AwesomeServer() {
		super();
		SessionStore.put(UserSocketMap.class, socketUserMap);
		
		Logger.getLogger("default").debug("Creating RSA Cipher Suite");
		try {
			cipherSuite = RSACipherSuite.createCipherSuite();
		} catch (final Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Logger.getLogger("default").fatal("Could not create cipher suite");
			
			cipherSuite = new NoCipherSuite();
		}
	}
	
	@Override
	public void start(final int port) throws IOException {
		super.start(port);
		
		beginAcceptAndReceive(this, this, this);
		
		isRunning = true;
	}
	
	/**
	 * Stops the server
	 */
	public void stop() {
		endReceive();
		endAccept();

		isRunning = false;
	}
	
	/**
	 * Indicates if the server is running
	 * @return true if server is running
	 */
	public boolean isRunning() {
		return isRunning;
	}

	@Override
	public void clientAccepted(final Socket acceptedSocket) {
		if (isRunning) {
			final User socketUser = userManager.createUser();
			socketUser.setUserState(UserState.ONLINE);
			
			Logger.getLogger("default").debug("Setting ciphersuite to default (no cipher");
			socketCipherMap.put(acceptedSocket, new NoCipherSuite()); // Put No Cipher suite in for transferring rsa key
			socketUserMap.put(socketUser, acceptedSocket);
			userManager.addUser(socketUser);
			
			// TODO: Move channel operations to different state of connection
			/*
			final Channel defaultChannel = channelManager.getDefaultChannel();
			if (defaultChannel != null) {
				defaultChannel.addUser(socketUser);
			}
			
			final JSONObject responseArg = (new JSONObject()).add("Message", 
				(new JSONObject())
					.add("userID", socketUser.getUserID()));
			
			final Message response = new Message(MessageType.CONNECTRESPONSE, responseArg);
			*/

			try {
				final JSONObject responseArg = (new JSONObject()).add("Message", cipherSuite.getPublicKeyObject());
				final Message response = new Message(MessageType.KEYTRANSFER, responseArg);
			
				send(acceptedSocket, response.toPacket());
				socketCipherMap.put(acceptedSocket, cipherSuite); // Put RSA Cipher suite in for initial encryption of the clients aes key
			} catch (final Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void packetReceived(final Socket sender, final Packet packet) {
		if (isRunning) {
			final User senderUser = socketUserMap.findKey(sender);
			final ICipherSuite senderCipherSuite = socketCipherMap.get(sender);
			try {
				final Packet decryptedPacket = CipherHelper.decryptPacket(packet, senderCipherSuite);
				final Message msg = Message.fromPacket(decryptedPacket);
				
				Logger.getLogger("default").debug("Received Message: %s", msg.getMessageContent());
				
				if (msg.getMessageType() == MessageType.KEYTRANSFER) {
					Logger.getLogger("default").debug("Getting AES Key from client");
				}
				
				synchronized (messageQueue) {
					messageQueue.add(new PriorityQueueElement(msg, sender, senderUser));
				}
				startQueue();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void send(final Socket socket, final Packet packet) throws IOException { 
		if (socketCipherMap.containsKey(socket)) {
			final ICipherSuite cipherSuite = socketCipherMap.get(socket);
			try {
				final Packet encryptedPacked = CipherHelper.encryptPacket(packet, cipherSuite);
				super.send(socket, encryptedPacked);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Logger.getLogger("default").fatal("Could not encrypt packet for socket %s", socket);
			}
		} else {
			Logger.getLogger("default").error("No cipher suite linked to the client %s", socket);
		}
	}

	@Override
	public void eventTriggered(final Object sender, final Socket socket) {
		// Client disconnects
		final User user = socketUserMap.findKey(socket);
		Logger.getLogger("default").debug("Client disconnected %s, %s", socket, user);
		if (user != null) {
			user.setUserState(UserState.OFFLINE);
			// TODO: Send message to users that user is offline
			final Iterator<Channel> channels = channelManager.userJoinedChannels(user);
			while (channels.hasNext()) {
				final Channel channel = channels.next();
				channel.leave(user);
			}
		}
	}
	
	/**
	 * Start new thread to process messages 
	 */
	private void startQueue() {
		Logger.getLogger("default").debug("runningThreads: %s", runningThreads);
		Logger.getLogger("default").debug("lastQueueWork:  %s", lastQueueWork);
		
		if (runningThreads == 0 || tooMuchLoad || (lastQueueWork + QUEUE_TIMEOUT < System.currentTimeMillis())) {
			queueProcessingService.execute(new Runnable() {
				@Override
				public void run() {
					runningThreads++;
					int size;
					synchronized (messageQueue) {
						size = messageQueue.size();	
					}
					final int oldSize = size;
					PriorityQueueElement workingElement;
					while (size > 0) {
						synchronized (messageQueue) {
							workingElement = messageQueue.poll();
						}
						if (workingElement != null) {
							Logger.getLogger("default").info("Processing queue (%s)", Thread.currentThread().getName());
							Logger.getLogger("default").debug("     %s - %s", workingElement.getSenderUser(), workingElement.getMessage());
							lastQueueWork = System.currentTimeMillis();
							processQueue(workingElement);
						}
						synchronized (messageQueue) {
							size = messageQueue.size();	
						}
						tooMuchLoad = (size > (oldSize + MAX_LOAD_INCREASE)) && (size > CARE_ABOUT_LOAD_QUEUE_SIZE);
						
						if (tooMuchLoad) {
							Logger.getLogger("default").debug("Too much load (%s)", runningThreads);
						}
					}
					runningThreads--;
					//tooMuchLoad = false;
				}
			});	
		}
	}
	
	/**
	 * Processes an element from the queue
	 * @param elem The element to process
	 */
	private void processQueue(final PriorityQueueElement elem) {
		try {
			final Message request = elem.getMessage();
			final User senderUser = elem.getSenderUser();
			final Socket sender = elem.getSender();
			
			final Message response = MessageProcessor.process(request, senderUser, sender);
			
			if (response != null) {
				// If message is a real user message, multicast to others in channel otherwise only send response back to sender
				if (response.getMessageType() == MessageType.MESSAGE) {
					// If user really exists and the response has the correct type to proceed multicasting
					if (senderUser != null && response instanceof MulticastMessage) {
						// Get all Users who should receive the message
						final User[] multicastTo = ((MulticastMessage)response).getReceiverArray();
						// The packet to be sent to all users
						final Packet multicastPacket = response.toPacket();
						final Iterator<Socket> receiver = socketUserMap.values(multicastTo);
						while (receiver.hasNext()) {
							send(receiver.next(), multicastPacket);
						}
					}
				} else {
					send(sender, response.toPacket());
				}
			}
		} catch (final IOException e) {
			e.printStackTrace();
			return;
		}
	}
}
