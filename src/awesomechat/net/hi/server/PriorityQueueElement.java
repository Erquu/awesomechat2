package awesomechat.net.hi.server;

import java.net.Socket;

import awesomechat.net.messaging.Message;
import awesomechat.util.usermanagement.User;

public class PriorityQueueElement implements Comparable<PriorityQueueElement> {
	/** Instance of the message to process*/
	private final Message message;
	/** Sender of the message */
	private final Socket sender;
	/** User who sent the message */
	private final User senderUser;
	/** instance creation timestamp */
	private final long timestamp;
	
	/**
	 * Creates a new queue element
	 * @param message The instance of the message to process
	 * @param sender The sender socket of the message
	 * @param senderUser The sender user of the message
	 */
	public PriorityQueueElement(final Message message, final Socket sender, final User senderUser) {
		this.message = message;
		this.sender = sender;
		this.senderUser = senderUser;
		this.timestamp = System.currentTimeMillis();
	}

	/**
	 * @return The message
	 */
	public Message getMessage() {
		return message;
	}

	/**
	 * @return The sender socket
	 */
	public Socket getSender() {
		return sender;
	}

	/**
	 * @return The sender user
	 */
	public User getSenderUser() {
		return senderUser;
	}

	@Override
	public int compareTo(final PriorityQueueElement arg0) {
		int l = message.getMessageType().ordinal();
		int r = arg0.getMessage().getMessageType().ordinal();
		if (l == r) {
			return (int) (timestamp - arg0.timestamp);
		}
		return (l - r);
	}
}
