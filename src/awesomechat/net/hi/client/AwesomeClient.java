package awesomechat.net.hi.client;

import java.io.IOException;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import logging.Logger;
import awesomechat.io.json.JSONArray;
import awesomechat.io.json.JSONObject;
import awesomechat.net.Packet;
import awesomechat.net.hi.client.events.IChannelEventHandler;
import awesomechat.net.hi.client.events.IMessageEventHandler;
import awesomechat.net.hi.client.events.IUserEventHandler;
import awesomechat.net.hi.sec.CipherHelper;
import awesomechat.net.hi.sec.ICipherSuite;
import awesomechat.net.hi.sec.impl.AESCipherSuite;
import awesomechat.net.hi.sec.impl.NoCipherSuite;
import awesomechat.net.hi.sec.impl.RSACipherSuite;
import awesomechat.net.lo.callback.IReceiveCallback;
import awesomechat.net.lo.client.AsyncTcpClient;
import awesomechat.net.messaging.Message;
import awesomechat.net.messaging.MessageType;
import awesomechat.util.usermanagement.User;

public class AwesomeClient extends AsyncTcpClient implements IReceiveCallback<Socket> {
	/** The user registered on the server */
	protected User localUser = null;
	
	protected IUserEventHandler userEventHandler = null;
	protected IMessageEventHandler messageEventHandler = null;
	protected IChannelEventHandler channelEventHandler = null;
	
	/** The ICipherSuite used for encryption and decryption */
	private ICipherSuite aesSuite = new NoCipherSuite();
	private ICipherSuite activeSuite = new NoCipherSuite();

	public AwesomeClient(final InetAddress remoteAddress, final int port) {
		super(remoteAddress, port);
	}
	public AwesomeClient(final String remoteAddress, final int port)
			throws UnknownHostException {
		super(remoteAddress, port);
	}
	
	public void registerUserEventHandler(final IUserEventHandler handler) {
		userEventHandler = handler;
	}
	
	public void registerMessageEventHandler(final IMessageEventHandler handler) {
		messageEventHandler = handler;
	}
	
	public void registerChannelEventHandler(final IChannelEventHandler handler) {
		channelEventHandler = handler;
	}
	
	@Override
	public void send(final Packet packet) {
		try {
			final Packet encryptedPacket = CipherHelper.encryptPacket(packet, activeSuite);
			super.send(encryptedPacket);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Logger.getLogger("default").error("Could nont encrypt packet");
		}
	}
	
	@Override
	public void serverDisconnected(final boolean planned) {
		super.serverDisconnected(planned);
		
		for (int i = 0; i < 10; i++) {
			try {
				connect();
				break;
			} catch (IOException e) {
				Logger.getLogger("default").error("Could not reconnect to the server");
				//e.printStackTrace();
			}
		}
	}
	
	@Override
	public void connect() throws IOException {
		super.connect();
		
		beginReceive(this);
	}
	
	@SuppressWarnings("incomplete-switch")
	@Override
	public void packetReceived(final Socket sender, final Packet packet) {
		try {
			final Logger logger = Logger.getLogger("default");
			
			final Packet decryptedPacket;
			try {
				decryptedPacket = CipherHelper.decryptPacket(packet, activeSuite);
			} catch (final Exception e) {
				// TODO: handle exception
				logger.error("Could not decrypt packet");
				e.printStackTrace();
				return;
			}
			
			final Message msg = Message.fromPacket(decryptedPacket);
			final JSONObject arg = msg.getMessageContent().get("Message");
			
			if (msg.getMessageType() == MessageType.UNKNOWN) {
				logger.debug("UNKNOWN MESSAGE TYPE");
			}
			
			switch (msg.getMessageType()) {
				case MESSAGE:
					{
						final String userID = arg.getString("userID");
						final String channelName = arg.getString("channelName");
						final String messageText = arg.getString("message");
						if (messageEventHandler != null) {
							messageEventHandler.messageReceived(userID, channelName, messageText);
						}
					}
					break;
				case KEYTRANSFER:
					logger.debug("Received public key from server");

					// Create servers public key to encrypt 
					final BigInteger mod = new BigInteger(arg.getString("mod"));
					final BigInteger exp = new BigInteger(arg.getString("exp"));
					try {
						activeSuite = RSACipherSuite.createEncryptOnly(mod, exp);
					} catch (final Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						logger.fatal("Could not create server cipher suite");
					}
										
					try {
						logger.debug("Creating local AES Cipher Suite");
						aesSuite = AESCipherSuite.createCipherSuite();

						final JSONObject responseArg = (new JSONObject()).add("Message", aesSuite.getPublicKeyObject());
						final Message response = new Message(MessageType.KEYTRANSFER, responseArg);
						send(response.toPacket());
					} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						logger.fatal("Could not create local cipher suite");
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						logger.error("Could not create public key packet");
					}
					
					break;
				case CONNECTRESPONSE:
					localUser = new User(arg.getString("userID"));
					logger.info("Logged in as %s", localUser);
					if (userEventHandler != null) {
						userEventHandler.userLoggedIn(localUser);
					}
					break;
				case LOGINRESPONSE:
					{
						if (Boolean.parseBoolean(arg.getString("success"))) {
							final String userID = arg.getString("userID");
							String displayName = null;
							if (arg.get("userData") instanceof JSONObject && arg.getString("userData", "displayName") != null) {
								displayName = arg.getString("userData", "displayName");
							}
							final User newUser = new User(userID, displayName);
							logger.info("User logged in %s=>%s", localUser, newUser);
							localUser = newUser;
							if (userEventHandler != null) {
								userEventHandler.userLoggedIn(localUser);
							}
						} else {
							final String message = arg.get("message").toString();
							logger.info("Failed to login: %s", message);
							if (userEventHandler != null) {
								userEventHandler.userLoginFailed(message);
							}
						}
					}
					break;
				case CHANNELCREATERESPONSE:
					{
						final String channelName = arg.getString("channelName");
						if (Boolean.parseBoolean(arg.getString("success"))) {
							logger.info("Created channel %s", channelName);
							if (channelEventHandler != null) {
								channelEventHandler.channelCreated(channelName);
							}
						} else {
							final String message = arg.getString("message");
							logger.info("Failed to create channel %s: %s", channelName, message);
							if (channelEventHandler != null) {
								channelEventHandler.channelCreationFailed(channelName, message);
							}
						}
					}
					break;
				case CHANNELJOINRESPONSE:
					{
						final String channelName = arg.getString("channelName");
						final String message = arg.getString("message");
						
						if (Boolean.parseBoolean(arg.getString("success"))) {
							logger.info("Joined channel %s, %s", channelName, message);
							if (channelEventHandler != null) {
								channelEventHandler.channelJoined(channelName, message);
							}
						} else {
							logger.info("Failed to join channel %s: %s", channelName, message);
							if (channelEventHandler != null) {
								channelEventHandler.channelJoinFailed(channelName, message);
							}
						}
					}
					break;
				case CHANNELLEAVERESPONSE:
					{
						final String channelName = arg.getString("channelName");
						
						if (Boolean.parseBoolean(arg.getString("success"))) {
							logger.info("Left channel %s", channelName);
							if (channelEventHandler != null) {
								channelEventHandler.channelLeft(channelName);
							}
						} else {
							final String message = arg.getString("message");
							logger.info("Failed to leave channel %s: %s", channelName, message);
							if (channelEventHandler != null) {
								channelEventHandler.channelLeaveFailed(channelName, message);
							}
						}
					}
					break;
			case CHANNELUSERSYNCRESPONSE:
				{
					final String channelName = arg.get("channelName").toString();
					
					if (Boolean.parseBoolean(arg.get("success").toString())) {
						logger.info("Sync users in channel %s", channelName);
						
						final List<User> users = new Vector<User>();
						
						final JSONObject userListObject = arg.get("users");
						if (userListObject instanceof JSONArray) {
							final Iterator<JSONObject> userList = ((JSONArray)userListObject).iterator();
							while (userList.hasNext()) {
								final JSONObject userObject = userList.next();
								final String userID = userObject.getString("userID");
								final String displayName = userObject.getString("displayName");
								
								if (userID != null) {
									final User user = new User(userID, displayName); 
									users.add(user);
									logger.info("   %s", user);
								}
							}
						}
						
						if (channelEventHandler != null) {
							 channelEventHandler.channelUserSync(channelName, users.toArray(new User[users.size()]));
						}
					} else {
						final String message = arg.get("message").toString();
						logger.info("Failed to sync channel %s: %s", channelName, message);
					}
				}
				break;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
