package awesomechat.net.hi.client;

import java.net.InetAddress;
import java.net.UnknownHostException;

import awesomechat.io.json.JSONObject;
import awesomechat.net.messaging.Message;
import awesomechat.net.messaging.MessageType;

public class SimpleClient extends AwesomeClient {

	/**
	 * {@inheritDoc AwesomeClient#AwesomeClient(InetAddress, int)}
	 */
	public SimpleClient(final InetAddress remoteAddress, final int port) {
		super(remoteAddress, port);
	}

	/**
	 * {@inheritDoc AwesomeClient#AwesomeClient(String, int)}
	 */
	public SimpleClient(final String remoteAddress, final int port)
			throws UnknownHostException {
		super(remoteAddress, port);
	}
	
	
	public boolean sendMessage(final String message, final String channelName) {
		return send(MessageType.MESSAGE,
			(new JSONObject())
				.add("channelName", channelName != null ? channelName : "")
				.add("message", message));
	}
	
	public boolean login(final String userID, final String userPass) {
		return send(MessageType.LOGINREQUEST,
			(new JSONObject())
				.add("userID", "")
				.add("newUserID", userID)
				.add("password", userPass));
	}
	
	public boolean createChannel(final String channelName, final String channelPass, final String welcomeMessage) {
		return send(MessageType.CHANNELCREATEREQUEST,
			(new JSONObject())
				.add("userID", "")
				.add("channelName", channelName)
				.add("password", channelPass)
				.add("welcome", welcomeMessage));
	}
	
	public boolean joinChannel(final String channelName, final String channelPass) {
		return send(MessageType.CHANNELJOINREQUEST,
			(new JSONObject())
				.add("userID", "")
				.add("channelName", channelName)
				.add("password", channelPass));
	}
	
	public boolean leaveChannel(final String channelName) {
		return send(MessageType.CHANNELLEAVEREQUEST,
			(new JSONObject())
				.add("userID", "")
				.add("channelName", channelName));
	}

	private boolean send(final MessageType type, final JSONObject args) {
		final Message msg = new Message(type, (new JSONObject()).add("Message", args));
		// TODO: handle failcallback
		sendAsync(msg.toPacket(), null);
		return true;
	}
}
