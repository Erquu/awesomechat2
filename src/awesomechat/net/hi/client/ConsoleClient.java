package awesomechat.net.hi.client;

import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;

import awesomechat.io.json.JSONObject;
import awesomechat.net.hi.client.events.IMessageEventHandler;
import awesomechat.net.messaging.Message;
import awesomechat.net.messaging.MessageType;
import logging.Logger;

public class ConsoleClient extends SimpleClient implements IMessageEventHandler {
	
	interface IConsole {
		String readLine();
		String readLine(final String format, final Object... args);
		char[] readPassword(final String format, final Object... args);
		PrintWriter writer();
		boolean valid();
	}
	class NormalConsole implements IConsole {
		private final Console console;
		
		public NormalConsole() {
			console = System.console();
		}

		@Override
		public String readLine() {
			return console.readLine();
		}
		@Override
		public String readLine(final String format, final Object... args) {
			return console.readLine(format, args);
		}

		@Override
		public char[] readPassword(final String format, final Object... args) {
			return console.readPassword(format, args);
		}

		@Override
		public PrintWriter writer() {
			return console.writer();
		}

		@Override
		public boolean valid() {
			return console != null;
		}
	}
	class DebugConsole implements IConsole {

		private final BufferedReader reader;
		private final PrintWriter writer;
		
		public DebugConsole() {
			writer = new PrintWriter(System.out);
			reader = new BufferedReader(new InputStreamReader(System.in));
		}

		@Override
		public String readLine() {
			try {
				return reader.readLine();
			} catch (IOException e) {}
			return null;
		}
		@Override
		public String readLine(final String format, final Object... args) {
			writer.printf(format, args);
			try {
				return reader.readLine();
			} catch (IOException e) {}
			return null;
		}

		@Override
		public char[] readPassword(final String format, final Object... args) {
			writer.printf(format, args);
			try {
				return reader.readLine().toCharArray();
			} catch (IOException e) {}
			return null;
		}

		@Override
		public PrintWriter writer() {
			return writer;
		}

		@Override
		public boolean valid() {
			return true;
		}
		
	}
	
	private final Logger logger = Logger.getLogger("default");
	
	private IConsole console;
	private String activeChannel = "";
	
	private boolean enableAdminCommands = false;

	public ConsoleClient(final InetAddress remoteAddress, final int port) {
		super(remoteAddress, port);
	}
	public ConsoleClient(final String remoteAddress, final int port) throws UnknownHostException {
		super(remoteAddress, port);
	}

	@Override
	public void messageReceived(final String senderName, final String channelName, final String messageText) {
		logger.info(String.format("%s: %s", senderName, messageText));
	}
	
	public void start() throws IOException {
		console = new NormalConsole();
		if (!console.valid())
			console = new DebugConsole();
		
		registerMessageEventHandler(this);
		
		connect();
		
		String line;
		while (!"stop".equals(line = console.readLine().trim())) {
			final String[] args = line.split(" ");
			
			if (args.length > 0 && args[0].startsWith("/")) {
				switch (args[0].substring(1)) {
					case "admin":
						{
							final String adminPass = new String(console.readPassword("Password: "));
							if ("Acht42".equals(adminPass)) {
								enableAdminCommands = true;
								logger.info("Admin mode enabled");
							}
						}
						break;
					case "block":
						if (enableAdminCommands && args.length > 1) {
							final String timeout = args[1];
							send(new Message(MessageType.SYSTEM, (new JSONObject()).add("type", "block").add("timeout", timeout)).toPacket());
						}
						break;
					case "createchannel":
						{
							logger.info("Creating channel");
							
							final String channelName = console.readLine("Name: ");
							final String channelPass = new String(console.readPassword("Password: "));
							final String channelPassRepeat = new String(console.readPassword("Repeat password: "));
							if (channelPass.equals(channelPassRepeat)) {
								final String welcomeMessage = console.readLine("Welcome message: ");
								
								createChannel(channelName, channelPass, welcomeMessage);
							} else {
								console.writer().println("Passwords do not match!");
							}
						}
						break;
					case "join":
						{
							final String channelName;
							if (args.length > 1) {
								channelName = args[1];
							} else {
								channelName = console.readLine("Channelname: ");
							}
							final String channelPass = new String(console.readPassword("Password: "));
							joinChannel(channelName, channelPass);
						}
						break;
					case "leave":
						{
							final String channelName;
							if (args.length > 1) {
								channelName = args[1];
							} else {
								channelName = console.readLine("Channelname: ");
							}
							leaveChannel(channelName);
						}
						break;
					case "login":
						logger.info("Logging in");

						final String userID = console.readLine("Username: ");
						final String userPass = new String(console.readPassword("Password: "));
						
						login(userID, userPass);
						break;
				}
			} else {
				String channel = activeChannel;
				
				if (line.startsWith("@")) {
					int index = -1;
					if ((index = line.indexOf(" ")) > -1) {
						channel = line.substring(1, index);
						line = line.substring(index).trim();
					}
				}
				sendMessage(line, channel);
			}
		}
	}
}
