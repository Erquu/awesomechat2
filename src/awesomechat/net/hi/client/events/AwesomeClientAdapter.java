package awesomechat.net.hi.client.events;

import awesomechat.util.usermanagement.User;

public class AwesomeClientAdapter implements IChannelEventHandler, IMessageEventHandler, IUserEventHandler {
	@Override public void userLoggedIn(final User user) {}
	@Override public void userLoginFailed(final String message) {}
	@Override public void userLoggedOut() {}
	@Override public void messageReceived(final String senderName, final String channelName, final String messageText) {}
	@Override public void channelJoined(final String channelName, final String welcomeMessage) {}
	@Override public void channelJoinFailed(final String channelName, final String message) {}
	@Override public void channelLeft(final String channelName) {}
	@Override public void channelLeaveFailed(final String channelName, final String message) {}
	@Override public void channelCreated(final String channelName) {}
	@Override public void channelCreationFailed(final String channelName, final String message) {}
	@Override public void channelUserSync(final String channelName, final User[] users) {}
}
