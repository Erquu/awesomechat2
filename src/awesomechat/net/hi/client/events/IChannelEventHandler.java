package awesomechat.net.hi.client.events;

import awesomechat.util.usermanagement.User;

public interface IChannelEventHandler {
	void channelJoined(final String channelName, final String welcomeMessage);
	void channelJoinFailed(final String channelName, final String message);
	
	void channelLeft(final String channelName);
	void channelLeaveFailed(final String channelName, final String message);
	
	void channelCreated(final String channelName);
	void channelCreationFailed(final String channelName, final String message);
	
	void channelUserSync(final String channelName, final User[] users);
}
