package awesomechat.net.hi.client.events;

public interface IMessageEventHandler {
	public void messageReceived(final String senderName, final String channelName, final String messageText);
}
