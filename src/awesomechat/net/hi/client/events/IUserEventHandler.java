package awesomechat.net.hi.client.events;

import awesomechat.util.usermanagement.User;

public interface IUserEventHandler {
	void userLoggedIn(final User user);
	void userLoginFailed(final String message);
	void userLoggedOut();
}
