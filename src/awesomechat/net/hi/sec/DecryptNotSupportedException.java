package awesomechat.net.hi.sec;

public class DecryptNotSupportedException extends Exception {
	private static final long serialVersionUID = -7565585279222160613L;

	public DecryptNotSupportedException() {
		super("Decrypt is not supported due to a missing key");
	}
}
