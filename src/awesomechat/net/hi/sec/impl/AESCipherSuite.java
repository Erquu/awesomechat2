package awesomechat.net.hi.sec.impl;

import logging.Logger;
import awesomechat.io.json.JSONObject;
import awesomechat.net.hi.sec.ICipherSuite;

// TODO: Implement actual AES :)
public class AESCipherSuite implements ICipherSuite {
	
	private static final int AESKEYLENGTH = 16;
	
	public static AESCipherSuite createCipherSuite() {
		final char[] chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789,;.:#!�$%&/()=?{[]}<>|".toCharArray();
		final char[] key = new char[AESKEYLENGTH];
		
		for (int i = 0; i < key.length; i++) {
			int rand = (int)(Math.random() * (double)chars.length);
			key[i] = chars[rand];
		}
		
		return new AESCipherSuite(key);
	}
	public static AESCipherSuite createCipherSuite(final String key) {
		return new AESCipherSuite(key.toCharArray());
	}
	
	private final char[] key;
	private final int keyLength;
	
	private AESCipherSuite(final char[] key) {
		this.key = key;
		this.keyLength = this.key.length;
	}

	@Override
	public byte[] encrypt(final byte[] message) throws Exception {
		
		Logger.getLogger("default").debug("Encrypting %sB using AES", message.length);
		
		final byte[] outBuffer = new byte[message.length]; 
		for (int i = 0; i < outBuffer.length; i++) {
			outBuffer[i] = (byte)(message[i] ^ key[(i % keyLength)]);
		}
		
		Logger.getLogger("default").debug("Encrypted using AES b:%sB m:%sB", message.length, outBuffer.length);
		
		return outBuffer;
	}

	@Override
	public byte[] decrypt(final byte[] bytes) throws Exception {
		
		Logger.getLogger("default").debug("Decrypting %sB using AES", bytes.length);
		
		final byte[] outBuffer = new byte[bytes.length]; 
		for (int i = 0; i < outBuffer.length; i++) {
			outBuffer[i] = (byte)(bytes[i] ^ key[(i % keyLength)]);
		}
		
		Logger.getLogger("default").debug("Decrypted using AES b:%sB m:%sB", bytes.length, outBuffer.length);
		
		return outBuffer;
	}

	@Override
	public JSONObject getPublicKeyObject() throws Exception {
		return (new JSONObject()).add("key", new String(key));
	}

}
