package awesomechat.net.hi.sec.impl;

import awesomechat.io.json.JSONObject;
import awesomechat.net.hi.sec.ICipherSuite;

public class NoCipherSuite implements ICipherSuite {

	@Override
	public byte[] encrypt(byte[] message) throws Exception {
		return message;
	}

	@Override
	public byte[] decrypt(byte[] bytes) throws Exception {
		return bytes;
	}

	@Override
	public JSONObject getPublicKeyObject() {
		return new JSONObject();
	}

}
