package awesomechat.net.hi.sec.impl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import logging.Logger;
import awesomechat.io.json.JSONObject;
import awesomechat.net.hi.sec.DecryptNotSupportedException;
import awesomechat.net.hi.sec.EncryptNotSupportedException;
import awesomechat.net.hi.sec.ICipherSuite;

public class RSACipherSuite implements ICipherSuite {
	
	public static RSACipherSuite createEncryptOnly(final BigInteger mod, final BigInteger exp) throws InvalidKeySpecException, NoSuchAlgorithmException {
		final RSAPublicKeySpec keySpec = new RSAPublicKeySpec(mod, exp);
		final KeyFactory factory = KeyFactory.getInstance("RSA");
		PublicKey key = factory.generatePublic(keySpec);
		
		return new RSACipherSuite(null, key);
	}
	public static RSACipherSuite createCipherSuite() throws NoSuchAlgorithmException, InvalidKeySpecException, IOException {
		final KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
		//kpg.initialize(2048);
		kpg.initialize(4096);
		
		final KeyPair kp = kpg.genKeyPair();

		final Key privateKey = kp.getPrivate();
		final Key publicKey = kp.getPublic();
		
		return new RSACipherSuite(privateKey, publicKey);
		
		/*
		final KeyFactory factory = KeyFactory.getInstance("RSA");
		final RSAPublicKeySpec pub = factory.getKeySpec(publicKey, RSAPublicKeySpec.class);
		final RSAPrivateKeySpec priv = factory.getKeySpec(privateKey, RSAPrivateKeySpec.class);
		
		saveKey(priv.getModulus(), priv.getPrivateExponent());
		saveKey(pub.getModulus(), pub.getPublicExponent());
	}
	
	private static File saveKey(final BigInteger mod, final BigInteger exp) throws IOException {
		final File keyFile = File.createTempFile("", "");
		
		final ObjectOutputStream out = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(keyFile)));
		try {
			out.writeObject(mod);
			out.writeObject(exp);
		} catch (final Exception e) {
			e.printStackTrace();
		} finally {
			out.close();
		}
		
		return keyFile;
		*/
	}
	
	private final Key privateKey;
	private final Key publicKey;
	
	private RSACipherSuite(final Key privateKey, final Key publicKey) {
		this.privateKey = privateKey;
		this.publicKey = publicKey;
	}
	
	@Override
	public byte[] encrypt(final byte[] message) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, EncryptNotSupportedException, NoSuchProviderException {
		if (publicKey == null) {
			throw new EncryptNotSupportedException();
		}
		final Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.ENCRYPT_MODE, publicKey);
		final byte[] encrypted = cipher.doFinal(message);
		
		Logger.getLogger("default").debug("Encrypted using RSA b:%sB m:%sB", message.length, encrypted.length);
		
		return encrypted;
	}

	@Override
	public byte[] decrypt(final byte[] bytes) throws UnsupportedEncodingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, DecryptNotSupportedException, NoSuchProviderException {		
		if (privateKey == null) {
			throw new DecryptNotSupportedException();
		}
		final Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.DECRYPT_MODE, privateKey);
		final byte[] decrypted = cipher.doFinal(bytes);
		
		Logger.getLogger("default").debug("Decrypted using RSA b:%sB m:%sB", bytes.length, decrypted.length);
		
		return decrypted;
	}
	
	@Override
	public JSONObject getPublicKeyObject() throws InvalidKeySpecException, NoSuchAlgorithmException, NoSuchProviderException {
		final JSONObject keyObject = new JSONObject();
		
		final KeyFactory factory = KeyFactory.getInstance("RSA");
		final RSAPublicKeySpec pub = factory.getKeySpec(publicKey, RSAPublicKeySpec.class);
		
		keyObject.add("mod", pub.getModulus().toString());
		keyObject.add("exp", pub.getPublicExponent().toString());
		
		return keyObject;
	}
}
