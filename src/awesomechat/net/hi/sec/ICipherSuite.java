package awesomechat.net.hi.sec;

import awesomechat.io.json.JSONObject;

public interface ICipherSuite {
	byte[] encrypt(final byte[] message) throws Exception;
	byte[] decrypt(final byte[] bytes) throws Exception;
	
	JSONObject getPublicKeyObject() throws Exception;
}
