package awesomechat.net.hi.sec;

public class EncryptNotSupportedException extends Exception {
	private static final long serialVersionUID = 4727547578279565174L;

	public EncryptNotSupportedException() {
		super("Encrypt is not supported due to a missing key");
	}
}
