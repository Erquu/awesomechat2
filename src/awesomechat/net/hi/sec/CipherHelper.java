package awesomechat.net.hi.sec;

import awesomechat.net.Packet;

public class CipherHelper {
	private CipherHelper() { }
	
	public static Packet encryptPacket(final Packet packet, final ICipherSuite cipherSuite) throws Exception {
		//final byte[] buffer = packet.getBuffer();
		//return new Packet(cipherSuite.encrypt(buffer), packet.getOffset(), packet.getLength());
		final byte[] buffer = new byte[packet.getLength()];
		System.arraycopy(packet.getBuffer(), packet.getOffset(), buffer, 0, packet.getLength());
		return new Packet(cipherSuite.encrypt(buffer), 0, buffer.length);
	}
	
	public static Packet decryptPacket(final Packet packet, final ICipherSuite cipherSuite) throws Exception {
		final byte[] buffer = new byte[packet.getLength()];
		System.arraycopy(packet.getBuffer(), packet.getOffset(), buffer, 0, packet.getLength());
		return new Packet(cipherSuite.decrypt(buffer), 0, buffer.length);
	}
}
