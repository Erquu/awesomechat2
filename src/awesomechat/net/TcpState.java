package awesomechat.net;

/**
 * State of the TCP Connection
 * 
 * @author Erquu
 */
public enum TcpState {
	/** Connection is in an unknown state */
	UNKNOWN,
	
	/** Connection is waiting to be closed */
	CLOSEWAIT,
	/** Connection is closing */
	CLOSING,
	/** Connection is closed */
	CLOSED,
	
	/** Connection is established */
	ESTABLISHED,
	/** Socket is listening */
	LISTENING,
	
	/** Socket is sending a message */
	SENDING,
	
	/** Socket is waiting to receive a message */
	RECEIVEWAIT,
	/** Socket is receiving a message */
	RECEIVING
}
