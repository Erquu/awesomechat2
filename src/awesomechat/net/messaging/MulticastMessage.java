package awesomechat.net.messaging;

import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import awesomechat.util.usermanagement.User;

/**
 * A Message that holds multiple receivers<br />
 * This is needed to send a multicast message
 * 
 * @author Erquu
 */
public class MulticastMessage extends Message {
	/** The list holding all receivers for this message */
	protected final List<User> receiver = new Vector<User>();
	
	/**
	 * Adds an user (receiver) to the list
	 * @param user The user to be added
	 */
	public void addToReceiveList(final User user) {
		if (!receiver.contains(user)) {
			receiver.add(user);
		}
	}
	
	/**
	 * Returns an iterator over all added users
	 * @return An iterator over all added users
	 */
	public Iterator<User> receiver() {
		return receiver.iterator();
	}
	
	/**
	 * Returns an array of all added users
	 * @return An array of all added users
	 */
	public User[] getReceiverArray() {
		return receiver.toArray(new User[receiver.size()]);
	}
}
