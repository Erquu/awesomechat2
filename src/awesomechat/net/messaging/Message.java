package awesomechat.net.messaging;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

import awesomechat.io.json.JSONObject;
import awesomechat.io.json.JSONParser;
import awesomechat.net.Packet;

/**
 * Class representing a Message<br />
 * Messages are the "readable" form of {@link Packet}.
 * 
 * @author Erquu
 */
public class Message {
	/** Type of the message. */
	private MessageType messageType;
	/** String message */
	private JSONObject messageContent;
	
	/**
	 * Creates a new empty message
	 */
	public Message() {
		messageType = MessageType.UNKNOWN;
		messageContent = null;
	}
	/**
	 * Creates a new message with the given content
	 * @param messageType The type of the message
	 * @param messageString The content of the message
	 */
	public Message(final MessageType messageType, final JSONObject messageContent) {
		this.messageType = messageType;
		this.messageContent = messageContent;
	}
	
	/**
	 * Sets the type of this Message
	 * @param type The type of the message
	 */
	public void setMessageType(final MessageType type) {
		this.messageType = type;
	}
	/**
	 * Returns the type of the message
	 * @return The type of the message
	 */
	public MessageType getMessageType() {
		return messageType;
	}
	
	/**
	 * Sets the message content
	 * @param messageString The message content
	 */
	public void setMessageContent(final JSONObject messageContent) {
		this.messageContent = messageContent;
	}
	/**
	 * Returns the message content
	 * @return The message content
	 */
	public JSONObject getMessageContent() {
		return messageContent;
	}
	
	/**
	 * Converts the Message to a Packet
	 * @return The converted Message
	 */
	public Packet toPacket() {
		byte[] message = null;
		if (Charset.isSupported("UTF-8")) {
			try {
				message = messageContent.toString(false).getBytes("UTF-8");
			} catch (final UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		} 
		if (message == null){
			message = messageContent.toString(true).getBytes();
		}
		
		final byte[] packetBuffer = new byte[message.length + 1];
		packetBuffer[0] = (byte)messageType.ordinal();
		System.arraycopy(message, 0, packetBuffer, 1, packetBuffer.length-1);
		return new Packet(packetBuffer);
	}

	@Override
	public String toString() {
		return String.format("Message[messageType=%s, messageString=%s]", messageType, messageContent);
	}
	
	/**
	 * Creates a new Message from a Packet
	 * @param packet The packet to be converted
	 * @throws IOException 
	 */
	public static Message fromPacket(final Packet packet) throws IOException {
		final byte[] buffer = packet.getBuffer();
		final Message msg = new Message();
		msg.messageType = getEnum((int)buffer[packet.getOffset()]);
		final JSONParser parser = new JSONParser();
		msg.messageContent = parser.parse(new String(buffer, packet.getOffset() + 1, packet.getLength() - packet.getOffset() - 1, "UTF-8"));
		return msg;
	}
	
	/**
	 * Converts an MessageType-ordinal to the respective MessageType
	 * @param ordinal The ordinal of the MessageType
	 * @return The MessageType
	 */
	private static MessageType getEnum(final int ordinal) {
		for (final MessageType t : MessageType.values()) 
			if (t.ordinal() == ordinal)
				return t;
		return MessageType.UNKNOWN;
	}
}
