package awesomechat.net.messaging;

/**
 * The types of messages<br />
 * They are listed after their priorities from top (highest) to bottom (lowest)
 * 
 * @author Erquu
 */
public enum MessageType {
	/**
	 * <i>First response by the server when a client connects</i>
	 * <pre>
	 * {
	 *   "Message": {
	 *     "userID": "name"
	 *   }
	 * }
	 * </pre>
	 */
	CONNECTRESPONSE,
	
	/**
	 * <pre>
	 * {
	 *   "Message": {
	 *     "mod": long,
	 *     "exp": long
	 *   }
	 * }
	 * </pre>
	 * or
	 * <pre>
	 * {
	 *   "Message": {
	 *     "key": "key"
	 *   }
	 * }
	 * </pre>
	 */
	KEYTRANSFER,
	
	/**
	 * <pre>
	 * {
	 *   "Message": {
	 *     "newUserID": "name",
	 *     "password": "pass"
	 *   }
	 * }
	 * </pre>
	 */
	LOGINREQUEST,
	/**
	 * <pre>
	 * {
	 *   "Message": {
	 *     "userID": "The new userID"
	 *     "oldUserID": "The old userID",
	 *     "success": true/false,
	 *     "message": "message",
	 *     "userData": {
	 *       "displayName": "name"
	 *     }
	 *   }
	 * }
	 * </pre>
	 */
	LOGINRESPONSE,

	/**
	 * <pre>
	 * {
	 *   "Message": {
	 *     "channelName": "name",
	 *     "password": "pass",
	 *     "welcome": "welcome message"
	 *   }
	 * }
	 * </pre>
	 */
	CHANNELCREATEREQUEST,
	/**
	 * <pre>
	 * {
	 *   "Message": {
	 *     "userID": "name",
	 *     "channelName": "name",
	 *     "success": true/false,
	 *     "message": "message"
	 *   }
	 * }
	 * </pre>
	 */
	CHANNELCREATERESPONSE,
	
	/**
	 * <pre>
	 * {
	 *   "Message": {
	 *     "channelName": "name",
	 *     "password": "pass"
	 *   }
	 * }
	 * </pre>
	 */
	CHANNELJOINREQUEST,
	/**
	 * <pre>
	 * {
	 *   "Message": {
	 *     "userID": "name",
	 *     "channelName": "name",
	 *     "success": true/false,
	 *     "message": "message"
	 *   }
	 * }
	 * </pre>
	 */
	CHANNELJOINRESPONSE,
	
	/**
	 * <pre>
	 * {
	 *   "Message": {
	 *     "channelName": "name"
	 *   }
	 * }
	 * </pre>
	 */
	CHANNELLEAVEREQUEST,
	/**
	 * <pre>
	 * {
	 *   "Message": {
	 *     "userID": "name",
	 *     "channelName": "name",
	 *     "success": true/false,
	 *     "message": "message",
	 *   }
	 * }
	 * </pre>
	 */
	CHANNELLEAVERESPONSE,
	
	/**
	 * <pre>
	 * {
	 *   "Message": {
	 *     "channelName": "name"
	 *   }
	 * }
	 * </pre>
	 */
	CHANNELUSERSYNCREQUEST,
	/**
	 * <pre>
	 * {
	 *   "Message": {
	 *     "userID": "name",
	 *     "channelName": "name",
	 *     "success": true/false,
	 *     "message": "message",
	 *     "users": [
	 *       {
	 *         "userID": "id",
	 *         "displayName": "name",
	 *         "state": "ONLINE"
	 *       },
	 *       {
	 *         "userID": "id2",
	 *         "displayName": "name2",
	 *         "state": "AFK"
	 *       },
	 *       ...
	 *     ]
	 *   }
	 * }
	 * </pre>
	 */
	CHANNELUSERSYNCRESPONSE,
	
	/**
	 * <pre>
	 * {
	 *   "Message": {
	 *     "userID": "name",
	 *     "ip": "127.0.0.1",
	 *     "port": 12345
	 * }
	 * </pre>
	 */
	E2EREQUEST,
	
	/**
	 * <pre>
	 * {
	 *   "Message": {
	 *     "userID": "name",
	 *     "success": true/false
	 * }
	 * </pre>
	 */
	E2ERESPONSE,

	/**
	 * <pre>
	 * {
	 *   "Message": {
	 *     "userID": "name",
	 *     "channelName": true/false
	 * }
	 * </pre>
	 */
	E2EFAILRESPONSE,
	
	/**
	 * <b>Incoming:</b>
	 * <pre>
	 * {
	 *   "Message": {
	 *     "userID": "name",
	 *     "channelName": "name",
	 *     "message": "message"
	 *   }
	 * }
	 * </pre>
	 * <b>Outgoing:</b>
	 * <pre>
	 * {
	 *   "Message": {
	 *     "channelName": "name",
	 *     "message": "message"
	 *   }
	 * }
	 * </pre>
	 */
	MESSAGE,
	
	/**
	 * <i>Used for sending system relevant data</i> 
	 * <pre>
	 * {
	 *   type: "type",
	 *   [...]
	 * }
	 * <pre>
	 */
	SYSTEM,

	/**
	 * <pre>
	 * {
	 *   "Message": {
	 *     "userID": "name",
	 *     "channelName": "name",
	 *     "file": {
	 *       "name": "filename",
	 *       "hash": "md5hash",
	 *       "totalSize": size
	 *     }
	 *   }
	 * }
	 * </pre>
	 */
	FILETRANSFERREQUEST,

	/**
	 * <pre>
	 * {
	 *   "Message": {
	 *     "userID": "name",
	 *     "channelName": "name",
	 *     "transferID": id
	 *   }
	 * }
	 * </pre>
	 */
	FILETRANSFERRESPONSE,
	
	/**
	 * Not a JSON Object 
	 */
	FILEPART,
	
	UNKNOWN
}
