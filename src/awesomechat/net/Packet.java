package awesomechat.net;

/**
 * Class representing a network packet
 * 
 * @author Erquu
 */
public final class Packet {
	/** The data of the packet */
	private final byte[] buffer;
	/** The offset of the data in the buffer */
	private final int offset;
	/** The length of the data in the buffer */
	private final int length;
	
	/**
	 * Creates a new Packet
	 * @param buffer The data of the packet
	 */
	public Packet(final byte[] buffer) {
		this(buffer, 0, buffer.length);
	}
	/**
	 * Creates a new Packet
	 * @param buffer The data of the packet
	 * @param offset The offset of the data in the buffer
	 * @param length The length of the data in the buffer
	 */
	public Packet(final byte[] buffer, final int offset, final int length) {
		this.buffer = buffer;
		this.offset = offset;
		this.length = length;
	}
	
	/**
	 * Returns the data of the packet
	 * @return The data of the packet
	 */
	public byte[] getBuffer() {
		return buffer;
	}
	
	/**
	 * Returns the offset of the data in the buffer
	 * @return The offset of the data in the buffer
	 */
	public int getOffset() {
		return offset;
	}
	
	/**
	 * Returns the length of the data in the buffer
	 * @return The length of the data in the buffer
	 */
	public int getLength() {
		return length;
	}
}
