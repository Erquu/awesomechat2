package awesomechat.io;

import java.io.EOFException;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

public class CompressedBlockInputStream extends FilterInputStream {

	private byte[] inBuffer = null;
	private byte[] outBuffer = null;
	private int inLength = 0;
	private int outOffset = 0;
	private int outLength = 0;
	
	private Inflater inflater = null;
	
	public CompressedBlockInputStream(final InputStream inputStream) {
		super(inputStream);
		inflater = new Inflater();
	}
	
	private void readAndDecompress() throws IOException {
		int ch1 = in.read();
		int ch2 = in.read();
		int ch3 = in.read();
		int ch4 = in.read();
		
		if ((ch1 | ch2 | ch3 | ch4) < 0) 
			throw new EOFException();
		inLength = ((ch1 << 24) + (ch2 << 16) + (ch3 << 8) + (ch4 << 0));
		
		ch1 = in.read();
		ch2 = in.read();
		ch3 = in.read();
		ch4 = in.read();

		if ((ch1 | ch2 | ch3 | ch4) < 0) 
			throw new EOFException();
		outLength = ((ch1 << 24) + (ch2 << 16) + (ch3 << 8) + (ch4 << 0));
		
		if (inBuffer == null || (inLength > inBuffer.length)) {
			inBuffer = new byte[inLength];
		}
		if (outBuffer == null || (outLength > outBuffer.length)) {
			outBuffer = new byte[outLength];
		}
		
		int inOffset = 0;
		while (inOffset < inLength) {
			try {
				int inCount = in.read(inBuffer, inOffset, inLength - inOffset);
				if (inCount == -1)
					throw new EOFException();
				inOffset += inCount;
			} catch (ArrayIndexOutOfBoundsException e) {
				e.printStackTrace();
				break;
			}
		}
		
		inflater.setInput(inBuffer, 0, inLength);
		try {
			inflater.inflate(outBuffer);
		} catch (DataFormatException e) {
			throw new IOException("Data format exception - " + e.getMessage());
		}
		
		
		inflater.reset();
		outOffset = 0;
	}
	
	public int read() throws IOException {
		if (outOffset >= outLength) {
			try {
				readAndDecompress();
			} catch (EOFException e) {
				return -1;
			}
		}
		return outBuffer[outOffset++] & 0xFF;
	}
	
	public int read(final byte[] buffer, final int bufferOffset, final int bufferLength) throws IOException {
		int count = 0;
		while (count < bufferLength) {
			if (outOffset >= outLength) {
				try {
					if ((count > 0) && (in.available() == 0))
						return count;
					else
						readAndDecompress();
				} catch (EOFException e) {
					if (count == 0)
						count = -1;
					return count;
				}
			}
			
			final int toCopy = Math.min(outLength - outOffset, bufferLength - count);
			System.arraycopy(outBuffer, outOffset, buffer, bufferOffset + count, toCopy);
			outOffset += toCopy;
			count += toCopy;
		}
		
		return count;
	}
	
	@Override
	public int available() throws IOException {
		return (outLength - outOffset) + in.available();
	}

}
