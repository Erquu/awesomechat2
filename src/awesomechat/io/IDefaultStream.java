package awesomechat.io;

import java.io.InputStream;
import java.io.OutputStream;

public interface IDefaultStream {
	OutputStream createOutputStream(final OutputStream out);
	InputStream createInputStream(final InputStream in);
}
