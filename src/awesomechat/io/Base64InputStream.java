package awesomechat.io;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

import awesomechat.util.Base64;

/**
 * Input stream for encoding bas64
 * 
 * @author Erquu
 */
public class Base64InputStream extends FilterInputStream {

	/**
	 * Creates a new Base64InputStream
	 * @param inputStream The base input stream
	 */
	public Base64InputStream(final InputStream inputStream) {
		super(inputStream);
	}

	@Override
	public int read(final byte[] buffer, final int bufferOffset, final int bufferLength) throws IOException {
		final int size = super.read(buffer, bufferOffset, bufferLength);
		final byte[] tmpBuffer = new byte[size];
		System.arraycopy(buffer, 0, tmpBuffer, 0, size);
		
		final byte[] decoded = Base64.decode(tmpBuffer);
		System.arraycopy(decoded, 0, buffer, 0, decoded.length);
		return decoded.length;
	}
}
