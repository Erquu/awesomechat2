package awesomechat.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

public class StreamHelper {
	private final IDefaultStream defaultStream;
	
	private final Map<Socket, InputStream> socketInMap = new HashMap<Socket, InputStream>();
	private final Map<Socket, OutputStream> socketOutMap = new HashMap<Socket, OutputStream>();
	
	public StreamHelper() {
		this(new DefaultStream());
	}
	public StreamHelper(final IDefaultStream defaultStream) {
		this.defaultStream = defaultStream;
	}
	
	public void register(final Socket socket) throws IOException {
		if (!socketInMap.containsKey(socket))
			socketInMap.put(socket, defaultStream.createInputStream(socket.getInputStream()));
		if (!socketOutMap.containsKey(socket))
			socketOutMap.put(socket, defaultStream.createOutputStream(socket.getOutputStream()));
	}
	
	public void unregister(final Socket socket) {
		if (socketInMap.containsKey(socket))
			socketInMap.remove(socket);
		if (socketOutMap.containsKey(socket))
			socketOutMap.remove(socket);
	}
	
	public InputStream getInputStream(final Socket socket) {
		return socketInMap.get(socket);
	}
	
	public OutputStream getOutputStream(final Socket socket) {
		return socketOutMap.get(socket);
	}
}
