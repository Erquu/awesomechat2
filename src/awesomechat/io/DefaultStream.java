package awesomechat.io;

import java.io.InputStream;
import java.io.OutputStream;

public class DefaultStream implements IDefaultStream {

	@Override
	public OutputStream createOutputStream(final OutputStream out) {
		return out;
	}

	@Override
	public InputStream createInputStream(final InputStream in) {
		return in;
	}

}
