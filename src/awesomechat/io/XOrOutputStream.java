package awesomechat.io;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Xor encryption stream
 * 
 * @author Erquu
 */
public class XOrOutputStream extends FilterOutputStream {
	/** Checksum to xor with */
	private byte[] checksum;
	/** Length of the checksum */
	private int chkLength;
	/** The temporary input buffer */
	private byte[] inBuffer;
	/** Used space of the inBuffer */
	private int length = 0;

	/**
	 * Creates a new XOrOutputStream
	 * @param outputStream The base output stream
	 * @param size The maximum size of one packet
	 * @param checksum The checksum to xor with
	 */
	public XOrOutputStream(final OutputStream outputStream, final int size, final String checksum) {
		super(outputStream);
		this.inBuffer = new byte[size];
		this.checksum = checksum.getBytes();
		this.chkLength = this.checksum.length;
	}
	
	@Override
	public void write(final byte[] buffer) throws IOException {
		write(buffer, 0, buffer.length);
	}
	
	@Override
	public void write(final byte[] buffer, int bufferOffset, int bufferLength) throws IOException {
		while ((length + bufferLength) > inBuffer.length) {
			int toCopy = inBuffer.length - length;
			System.arraycopy(buffer, bufferOffset, inBuffer, length, toCopy);
			length += toCopy;
			send();
			bufferOffset += toCopy;
			bufferLength -= toCopy;
		}
		System.arraycopy(buffer, bufferOffset, inBuffer, length, bufferLength);
		length += bufferLength;
	}
	
	@Override
	public void flush() throws IOException {
		send();
		out.flush();
	}
	
	/**
	 * Writes the inBuffer to the base stream
	 * @throws IOException
	 */
	private void send() throws IOException {
		if (length > 0) {
			final byte[] outBuffer = new byte[length];
			for (int i = 0; i < length; i++) {
				outBuffer[i] = (byte) (inBuffer[i] ^ checksum[i % chkLength]);
			}
			
			out.write(outBuffer);
			out.flush();
			
			length = 0;
		}
	}
}
