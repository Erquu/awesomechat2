package awesomechat.io;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import awesomechat.util.Base64;

/**
 * Output stream for encoding to base64
 *  
 * @author Erquu
 */
public class Base64OutputStream extends FilterOutputStream {
	/** The temporary input buffer */
	private byte[] inBuffer;
	/** Used space of the inBuffer */
	private int length = 0;

	/**
	 * Creates a new Base64 Output stream
	 * @param outputStream The raw output stream
	 * @param size The buffer size
	 */
	public Base64OutputStream(final OutputStream outputStream, final int size) {
		super(outputStream);
		inBuffer = new byte[size];
	}
	
	@Override
	public void write(final byte[] buffer) throws IOException {
		write(buffer, 0, buffer.length);
	}
	
	@Override
	public void write(final byte[] buffer, int bufferOffset, int bufferLength) throws IOException {
		while ((length + bufferLength) > inBuffer.length) {
			final int toCopy = inBuffer.length - length;
			System.arraycopy(buffer, bufferOffset, inBuffer, length, toCopy);
			length += toCopy;
			send();
			bufferOffset += toCopy;
			bufferLength -= toCopy;
		}
		System.arraycopy(buffer, bufferOffset, inBuffer, length, bufferLength);
		length += bufferLength;
	}
	
	@Override
	public void flush() throws IOException {
		send();
		out.flush();
	}

	/**
	 * Encodes the data and writes them to the base stream
	 * @throws IOException Write or flush operation failed
	 */
	private void send() throws IOException {
		if (length > 0) {
			
			byte[] outBuffer = new byte[length];
			System.arraycopy(inBuffer, 0, outBuffer, 0, length);
			outBuffer = Base64.encodeToByte(outBuffer, false);
			
			out.write(outBuffer);
			out.flush();
			
			length = 0;
		}
	}
}
