package awesomechat.io;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.Deflater;

import logging.Logger;

public class CompressedBlockOutputStream extends FilterOutputStream {
	
	public static enum Level {
		/**
		 * The best and slowest compression level.  This tries to find very
		 * long and distant string repetitions.
		 */
		BEST_COMPRESSION(Deflater.BEST_COMPRESSION),
		/** The worst but fastest compression level. */
		BEST_SPEED(Deflater.BEST_SPEED),
		/** The default compression level. */
		DEFAULT_COMPRESSION(Deflater.DEFAULT_COMPRESSION),
		/** This level won't compress at all but output uncompressed blocks. */
		NO_COMPRESSION(Deflater.NO_COMPRESSION);
		
		private final int num;
		Level(final int num) {
			this.num = num;
		}
		
		public int getNum() {
			return num;
		}
	}
	
	public static enum Strategy {
		/** The default strategy. */
		DEFAULT_STRATEGY(Deflater.DEFAULT_STRATEGY),
		/**
		 * This strategy will not look for string repetitions at all.  It
		 * only encodes with Huffman trees (which means, that more common
		 * characters get a smaller encoding.
		 */
		HUFFMAN_ONLY(Deflater.HUFFMAN_ONLY),
		/**
		 * This strategy will only allow longer string repetitions.  It is
		 * useful for random data with a small character set.
		 */
		FILTERED(Deflater.FILTERED);
		
		private final int num;
		Strategy(final int num) {
			this.num = num;
		}
		
		public int getNum() {
			return num;
		}
	}

	private byte[] inBuffer = null;
	private byte[] outBuffer = null;
	private int length = 0;
	private Deflater deflater = null;
	
	public CompressedBlockOutputStream(final OutputStream outStream, final int size) {
		this(outStream, size, Deflater.DEFAULT_COMPRESSION, Deflater.DEFAULT_STRATEGY);
	}
	public CompressedBlockOutputStream(final OutputStream outStream, final int size, final CompressedBlockOutputStream.Level level, final CompressedBlockOutputStream.Strategy strategy) {
		this(outStream, size, level.getNum(), strategy.getNum());
	}
	public CompressedBlockOutputStream(final OutputStream outStream, final int size, final int level, final int strategy) {
		super(outStream);
		this.inBuffer = new byte[size];
		this.outBuffer = new byte[size+64];
		this.deflater = new Deflater(level);
		this.deflater.setStrategy(strategy);
	}
	
	private void compressAndSend() throws IOException {
		if (length > 0) {
			deflater.setInput(inBuffer, 0, length);
			deflater.finish();
			final int size = deflater.deflate(outBuffer);
			
			Logger.getLogger("default").debug("Sending %s bytes (compressed), raw %s bytes", size, length);
			
			out.write((size >> 24) & 0xFF);
			out.write((size >> 16) & 0xFF);
			out.write((size >>  8) & 0xFF);
			out.write((size >>  0) & 0xFF);
			
			out.write((length >> 24) & 0xFF);
			out.write((length >> 16) & 0xFF);
			out.write((length >>  8) & 0xFF);
			out.write((length >>  0) & 0xFF);
			
			out.write(outBuffer, 0, size);
			out.flush();
			
			length = 0;
			deflater.reset();
		}
	}
		
	public void write(final int b) throws IOException {
		inBuffer[length++] = (byte)b;
		if(length == inBuffer.length) {
			compressAndSend();
		}
	}
	public void write(final byte[] buffer, int bufferOffset, int bufferLength) throws IOException {
		while ((length + bufferLength) > inBuffer.length) {
			final int toCopy = inBuffer.length - length;
			System.arraycopy(buffer, bufferOffset, inBuffer, length, toCopy);
			length += toCopy;
			compressAndSend();
			bufferOffset += toCopy;
			bufferLength -= toCopy;
		}
		System.arraycopy(buffer, bufferOffset, inBuffer, length, bufferLength);
		length += bufferLength;
	}
	
	@Override
	public void flush() throws IOException {
		compressAndSend();
		out.flush();
	}

}
