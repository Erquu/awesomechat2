package awesomechat.io;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Xor decryption stream
 * 
 * @author Erquu
 */
public class XOrInputStream extends FilterInputStream {
	/** Checksum to xor with */
	private byte[] checksum;
	/** Length of the checksum */
	private int chkLength;
	
	/**
	 * Creates a new XOrInputStream
	 * @param inputStream The raw input stream
	 * @param checksum The checksum to xor with
	 */
	public XOrInputStream(final InputStream inputStream, final String checksum) {
		super(inputStream);
		
		this.checksum = checksum.getBytes();
		this.chkLength = this.checksum.length;
	}

	@Override
	public int read() throws IOException {
		int read = super.read();
		
		return (read ^ checksum[0]);
	}
	
	@Override
	public int read(final byte[] buffer, final int bufferOffset, final int bufferLength) throws IOException {
		
		int receiveLength = super.read(buffer, bufferOffset, bufferLength);
		
		for (int i = 0; i < receiveLength; i++) {
			buffer[i] = (byte)(buffer[i] ^ checksum[i % chkLength]);
		}
		
		return receiveLength;
	}

}
