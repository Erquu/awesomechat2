package awesomechat.io.json;

public enum JSONObjectType {
	OBJECT,
	ARRAY,
	LITERAL,
	TEXT
}