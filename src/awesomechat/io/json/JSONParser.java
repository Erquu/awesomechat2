package awesomechat.io.json;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * The {@link JSONParser} is a very simple parser for json documents<br />
 * it can handle well formed json documents, not well formed documents may lead to wrong results without an exception thrown
 *
 * @author Erquu
 */
public class JSONParser {
	/** The document to be parsed*/
	private String doc;
	/** The length of the document*/
	private int length;
	/** The index of the current char in the document */
	private int index = 0;
	
	/**
	 * Parses a json document
	 * @param stream The document stream to be read from
	 * @return The created {@link JSONObject}
	 * @throws IOException
	 */
	public JSONObject parse(final InputStream stream) throws IOException {
		final BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
		final StringBuilder builder = new StringBuilder();
		String line;
		while ((line = reader.readLine()) != null) {
			builder.append(line);
			builder.append('\n');
		}
		return parse(builder.toString());
	}
	/**
	 * Parses a json document
	 * @param doc The json document to be parsed
	 * @return The created {@link JSONObject}
	 * @throws IOException
	 */
	public JSONObject parse(final String doc) throws IOException {
		return parse(doc, null, false).values.get(null);
	}
	
	/**
	 * Parses a json document 
	 * @param doc The json document to be parsed
	 * @param parent The parent node of found child-nodes
	 * @param isArray Indicates if the following text should be parsed as an JSON-array
	 * @return The created {@link JSONObject}
	 * @throws IOException
	 */
	private JSONObject parse(final String doc, final JSONObject parent, final boolean isArray) throws IOException {
		this.doc = doc;
		this.length = doc.length();
		
		final JSONObject currentObj = isArray ? (new JSONArray()) : (new JSONObject());
		currentObj.parent = parent;
		boolean isKey = true;
		String lastValueKey = null;
		
		while (index < length) {
			final char current = doc.charAt(index++);
			switch (current) {
				case '/':
					if (doc.charAt(index) == '/') {
						lineEnd();
					} else if (doc.charAt(index) == '*') {
						commentEnd();
					}
					break;
				case '\'':
				case '"':
					final String nodeValue = stringEnd(current);
					if (isArray) {
						((JSONArray)currentObj).objects.add(new JSONTextNode(nodeValue));
					} else {
						if (isKey) {
							lastValueKey = nodeValue;
						} else {
							currentObj.values.put(lastValueKey, new JSONTextNode(nodeValue));
						}
					}
					break;
				case ':':
					isKey = !isKey;
					break;
				case ',':
					isKey = true;
					lastValueKey = null;
					break;
				case '[':
					currentObj.values.put(lastValueKey, parse(doc, currentObj, true));
					break;
				case '{':
					final JSONObject obj = parse(doc, currentObj, false);
					if (isArray) {
						((JSONArray)currentObj).objects.add(obj);
					} else {
						currentObj.values.put(lastValueKey, obj);
					}
					break;
				case ']':
				case '}':
					return currentObj;
				default:
					if (!Character.isWhitespace(current)) {
						if (!isKey) {
							currentObj.values.put(lastValueKey, new JSONLiteralNode(literalEnd()));
						}
					}
					break;
			}
		}
		
		return currentObj;
	}
	
	/**
	 * Goes to the end of a literal and returns it as a String<br />
	 * Literals can be true/false, null or numbers like 1, 1231, 3.1415
	 * @return The literal content
	 */
	private String literalEnd() {
		final StringBuilder builder = new StringBuilder();
		index--;
		while (index < length) {
			final char currentChar = doc.charAt(index);
			if (!Character.isLetterOrDigit(currentChar))
				break;
			builder.append(currentChar);
			index++;
		}
		return builder.toString();
	}
	
	/**
	 * Goes to the end of a string and returns the string content
	 * @param initChar The character of the string start identifier char
	 * @return The string content
	 */
	private String stringEnd(final char initChar) {
		final StringBuilder builder = new StringBuilder();
		while (index < length) {
			final char currentChar = doc.charAt(index++);
			if (currentChar == '\\') {
				builder.append(doc.charAt(index));
				index++;
				continue;
			}
			if (currentChar == initChar) {
				break;
			}
			builder.append(currentChar);
		}
		return builder.toString();
	}
	
	/**
	 * Goes to the end of the current line
	 */
	private void lineEnd() {
		while (index < length) {
			char currentChar = doc.charAt(index++);
			if (currentChar == '\n') {
				break;
			}
		}
	}
	
	/**
	 * Goes to the end of a multiline comment
	 */
	private void commentEnd() {
		while (index < length) {
			final char currentChar = doc.charAt(index++);
			if (currentChar == '*' && doc.charAt(index) == '/') {
				index ++;
				break;
			}
		}
	}
}
