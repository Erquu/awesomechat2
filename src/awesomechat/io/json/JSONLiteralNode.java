package awesomechat.io.json;


public class JSONLiteralNode extends JSONObject {
	protected final String content;
	
	public JSONLiteralNode(final String content) {
		this.content = content;
	}
	
	public double toDouble() {
		return Double.parseDouble(content);
	}
	public float toFloat() {
		return Float.parseFloat(content);
	}
	public boolean toBoolean() {
		return Boolean.parseBoolean(content);
	}
	public int toInt() {
		return Integer.parseInt(content);
	}
	
	@Override
	public JSONObjectType getType() {
		return JSONObjectType.LITERAL;
	}
	
	@Override
	public String toJSONString(final int depth, final boolean minified) {
		return content;
	}
}