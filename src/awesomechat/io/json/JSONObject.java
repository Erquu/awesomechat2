package awesomechat.io.json;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class JSONObject {
	
	private static final String INDENT_SEQUENCE = "  ";
	
	protected JSONObject parent;
	
	protected final Map<String, JSONObject> values = new HashMap<String, JSONObject>();
	
	public JSONObject() { }
	
	public JSONObjectType getType() {
		return JSONObjectType.OBJECT;
	}
	
	@Override
	public String toString() {
		return toJSONString(1, false);
	}
	
	public String toString(final boolean minified) {
		return toJSONString(1, minified);
	}
	
	public JSONObject get(final String key) {
		return values.get(key);
	}
	public JSONObject get(final String... keys) {
		JSONObject returnValue = this;
		for (int i = 0; i < keys.length; i++) {
			returnValue = returnValue.get(keys[i]);
			if (returnValue == null) {
				break;
			}
		}
		return returnValue;
	}
	
	public String getString(final String key) {
		return get(key).toString();
	}
	public String getString(final String... key) {
		final JSONObject obj = get(key);
		if (obj != null)
			return obj.toString();
		return null;
	}
	
	public int getInt(final String key) throws NumberFormatException {
		return Integer.parseInt(getString(key));
	}
	public int getInt(final String... key) throws NumberFormatException {
		return Integer.parseInt(getString(key));
	}
	
	public long getLong(final String key) throws NumberFormatException {
		return Long.parseLong(getString(key));
	}
	public long getLong(final String... key) throws NumberFormatException {
		return Long.parseLong(getString(key));
	}
	
	public float getFloat(final String key) throws NumberFormatException {
		return Float.parseFloat(getString(key));
	}
	public float getFloat(final String... key) throws NumberFormatException {
		return Float.parseFloat(getString(key));
	}
	
	public double getDouble(final String key) throws NumberFormatException {
		return Double.parseDouble(getString(key));
	}
	public double getDouble(final String... key) throws NumberFormatException {
		return Double.parseDouble(getString(key));
	}
	
	public boolean getBool(final String key) {
		return Boolean.parseBoolean(getString(key));
	}
	public boolean getBool(final String... key) {
		return Boolean.parseBoolean(getString(key));
	}
	
	public JSONObject add(final String key, final JSONObject value) {
		values.put(key, value);
		return this;
	}
	public JSONObject add(final String key, final String value) {
		if (value == null)
			return add(key, new JSONLiteralNode("null"));
		return add(key, new JSONTextNode(value));
	}
	public JSONObject add(final String key, final boolean value) {
		return add(key, new JSONLiteralNode(Boolean.toString(value)));
	}
	public JSONObject add(final String key, final int value) {
		return add(key, new JSONLiteralNode(Integer.toString(value)));
	}
	public JSONObject add(final String key, final long value) {
		return add(key, new JSONLiteralNode(Long.toString(value)));
	}
	public JSONObject add(final String key, final double value) {
		return add(key, new JSONLiteralNode(Double.toString(value)));
	}
	public JSONObject add(final String key, final float value) {
		return add(key, new JSONLiteralNode(Float.toString(value)));
	}
	
	public String toJSONString(final int depth, final boolean minified) {
		final String depthString = getDepthString(depth);
		final StringBuilder builder = new StringBuilder();
		
		builder.append("{");
		if (!minified) {
			builder.append("\n");
			builder.append(depthString);
		}
		final Iterator<String> keys = values.keySet().iterator();
		int i = 0;
		while (keys.hasNext()) {
			final String key = keys.next();
			builder.append(String.format("\"%s\": ", toValidJSONString(key)));
			builder.append(values.get(key).toJSONString(depth + 1, minified));
			if (++i < values.size())
				builder.append(",");
			if (!minified) {
				builder.append("\n");
				if (i < values.size()) {
					builder.append(depthString);
				}
			}
		}
		if (!minified)
			builder.append(getDepthString(depth - 1));
		builder.append("}");
		return builder.toString();
	}
	
	protected String getDepthString(final int depth) {
		final StringBuilder depthString = new StringBuilder();
		for (int i = 0; i < depth; i++) {
			depthString.append(INDENT_SEQUENCE);
		}
		return depthString.toString();
	}
	
	protected String toValidJSONString(final String str) {
		if (str == null)
			return str;
		return str.replace("\"", "\\\"");
	}
}