package awesomechat.io.json;

/**
 * The {@link JSONTextNode} is a {@link JSONLiteralNode} with the difference, that
 * 
 * @author Erquu
 */
public class JSONTextNode extends JSONLiteralNode {
	
	public JSONTextNode(String content) {
		super(content);
	}

	@Override
	public JSONObjectType getType() {
		return JSONObjectType.TEXT;
	}
	
	@Override
	public String toString() {
		return content;
	}
	
	@Override
	public String toJSONString(final int depth, final boolean minified) {
		return String.format("\"%s\"", toValidJSONString(content));
	}
}