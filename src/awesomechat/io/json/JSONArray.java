package awesomechat.io.json;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class JSONArray extends JSONObject {
	protected final List<JSONObject> objects = new ArrayList<JSONObject>();
	
	@Override
	public JSONObjectType getType() {
		return JSONObjectType.ARRAY;
	}
	
	public JSONArray add(final JSONObject obj) {
		objects.add(obj);
		return this;
	}
	
	public JSONObject get(final int index) {
		return objects.get(index);
	}
	
	public int size() {
		return objects.size();
	}
	
	public Iterator<JSONObject> iterator() {
		return objects.iterator();
	}
	
	@Override
	public String toJSONString(final int depth, final boolean minified) {
		final String depthString = getDepthString(depth);
		final StringBuilder builder = new StringBuilder();
		
		builder.append("[");
		if (!minified) {
			builder.append("\n");
			builder.append(depthString);
		}
		int i = 0;
		for (final JSONObject obj : objects) {
			builder.append(obj.toJSONString(depth + 1, minified));
			if (++i < objects.size())
				builder.append(",");
			if (!minified) {
				builder.append("\n");
				if (i < objects.size()) {
					builder.append(depthString);
				}
			}
		}
		if (!minified)
			builder.append(getDepthString(depth - 1));
		builder.append("]");
		return builder.toString();
	}
}