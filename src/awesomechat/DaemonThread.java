package awesomechat;

/**
 * Normal Thread with deamon flag set to true.<br />
 * Daemon flags will be stopped/interrupted on program exit
 * 
 * @author Erquu
 */
public class DaemonThread extends Thread {
	public DaemonThread(final Runnable action) {
		super(action);
		setDaemon(true);
	}
}
