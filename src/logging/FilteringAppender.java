package logging;

import java.io.PrintStream;
import java.util.List;
import java.util.Vector;

public class FilteringAppender extends Appender {
	
	private final List<String> fqns = new Vector<String>();

	public FilteringAppender(final PrintStream writer) {
		super(writer);
	}
	public FilteringAppender(final PrintStream writer, final LogLevel logLevel) {
		super(writer, logLevel);
	}
	
	public void addLoggingFile(final String fqn) {
		fqns.add(fqn);
	}
	
	public boolean isLogging(final String fqn) {
		return fqns.contains(fqn);
	}

}
