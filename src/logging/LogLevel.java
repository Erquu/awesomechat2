package logging;

public enum LogLevel {
	NONE	(0),
	INFO	(1),
	WARN	(2),
	DEBUG	(4),
	ERROR	(8),
	FATAL	(16),
	
	/** INFO, WARN, ERROR, FATAL */
	PRODUCTIVE (INFO.toInteger() | WARN.toInteger() | ERROR.toInteger() | FATAL.toInteger()),
	/** ERROR, FATAL */
	BAD (ERROR.toInteger() | FATAL.toInteger()),
	/** INFO, WARN, DEBUG */
	GOOD (INFO.toInteger() | WARN.toInteger() | DEBUG.toInteger()),
	
	ALL (INFO.toInteger() | WARN.toInteger() | DEBUG.toInteger() | ERROR.toInteger() | FATAL.toInteger());
	
	private final int logLevel;
	
	LogLevel(final int num) {
		this.logLevel = num;
	}
	
	public int toInteger() {
		return logLevel;
	}
}
