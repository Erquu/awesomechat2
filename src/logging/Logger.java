package logging;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

public class Logger {
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MMM yyyy - HH:mm:ss");
	
	private static Map<String, Logger> loggerMap;
	
	public static Logger getLogger(String name) {
		if (loggerMap == null) {
			loggerMap = new HashMap<String, Logger>();
		}
		
		name = name.toLowerCase();
		if (!loggerMap.containsKey(name)) {
			loggerMap.put(name, new Logger(name));
		}
		return loggerMap.get(name);
	}
	
	
	private String loggerName = "";
	private final List<Appender> appenderList = new Vector<Appender>();
	
	public Logger(final String name) {
		this.loggerName = name;
	}
	
	public <T extends Appender> T addAppender(final T appender) {
		if (!appenderList.contains(appender)) {
			appenderList.add(appender);
		}
		return appender;
	}
	
	public void removeAppender(final Appender appender) {
		if (appenderList.contains(appender)) {
			appenderList.remove(appender);
		}
	}

	public String getLoggerName() {
		return loggerName;
	}
	
	public void info(final Object message) {
		writeln(LogLevel.INFO, message);
	}
	public void info(final String format, final Object... args) {
		info(String.format(format, args));
	}

	public void warn(final Object message) {
		writeln(LogLevel.WARN, message);
	}
	public void warn(final String format, final Object... args) {
		warn(String.format(format, args));
	}

	public void debug(final Object message) {
		writeln(LogLevel.DEBUG, message);
	}
	public void debug(final String format, final Object... args) {
		debug(String.format(format, args));
	}

	public void error(final Object message) {
		writeln(LogLevel.ERROR, message);
	}
	public void error(final String format, final Object... args) {
		error(String.format(format, args));
	}

	public void fatal(final Object message) {
		writeln(LogLevel.FATAL, message);
	}
	public void fatal(final String format, final Object... args) {
		error(String.format(format, args));
	}
	
	public String arrayValues(final Object[] arr) {
		return arrayValues(arr, 0);
	}
	public String arrayValues(final Object[] arr, final int offset) {
		return arrayValues(arr, offset, arr.length);
	}
	public String arrayValues(final Object[] arr, int offset, int length) {
		if (length < 0)
			return "";
		
		if (offset < 0)
			offset = 0;
		if (length > arr.length)
			length = arr.length;
		
		final StringBuilder builder = new StringBuilder();
		for (int i = offset; i < length; i++) {
			builder.append(String.format("[%s]", arr[i]));
			if (i+1 < arr.length)
				builder.append(",");
		}
		return builder.toString();
	}
	
	private void writeln(final LogLevel level, final Object message) {
		String typeString = level.name().toUpperCase();
		for (int i = 0, length = typeString.length(); i < 5 - length; i++) {
			typeString += " ";
		}
		final String dateString = dateFormat.format((new GregorianCalendar()).getTime());
		String stackTraceLine = "";

		
		final StackTraceElement[] stackTraces = Thread.currentThread().getStackTrace();
		StackTraceElement stackElement = null;
		String className = null;
		for (int i = 3; i < stackTraces.length; i++) {
			stackElement = stackTraces[i];
			className = stackElement.getClassName();
			className = className.substring(className.lastIndexOf(".")+1);
			className = className.replaceAll("(^$)*\\$\\d+", "");
			if (!className.equals("Logger")) {
				break;
			}
		}
		if (stackElement != null) {
			stackTraceLine = String.format("%s.java:%s", className, stackElement.getLineNumber());
			String line = String.format("%s - [%s] (%s) %s", typeString, dateString, stackTraceLine, message);
			
			for (final Appender appender : appenderList) {
				if (appender instanceof FilteringAppender
						&& !((FilteringAppender)appender).isLogging(stackElement.getClassName())) {
					continue;
				}
				if (appender.isLogging(level)) {
					appender.writeln(line);
				}
			}
		}
	}
}
