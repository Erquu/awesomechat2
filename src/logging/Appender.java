package logging;

import java.io.PrintStream;

public class Appender {
	private LogLevel logLevel = LogLevel.PRODUCTIVE;
	private PrintStream writer = null;
	
	public Appender(final PrintStream writer) {
		this.writer = writer;
	}
	public Appender(final PrintStream writer, final LogLevel logLevel)  {
		this(writer);
		this.logLevel = logLevel;
	}
	
	public void setLogLevel(final LogLevel logLevel) {
		this.logLevel = logLevel;
	}
	public LogLevel getLogLevel() {
		return this.logLevel;
	}
	
	public void setWriter(final PrintStream writer) {
		this.writer = writer;
	}
	public PrintStream getWriter() {
		return this.writer;
	}
	
	public boolean isLogging(final LogLevel level) {
		return ((level.toInteger() & logLevel.toInteger()) == level.toInteger());
	}
	
	public void writeln(final String str) {
		writer.println(str);
		//writer.flush();
	}
}
